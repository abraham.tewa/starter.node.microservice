// ============================================================
// Import packages
import express from 'express';
import { v4 as uuidV4 } from 'uuid';
import winston, { Logger } from 'winston';

import { MongoClient } from 'mongodb';
import type { Router } from 'express';

// ============================================================
// Import modules
import * as model from '../model';
import * as util from '../util';
import ApplicationConfiguration, { ToolConfiguration } from '../config/type';
import type {
    DynamicMeta,
    IProcessConfig,
    ProcessDeclaration,
    UUID,
} from '../types';

// ============================================================
// Class
class Service {
    readonly app: express.Express;

    readonly createdAt = new Date();

    readonly config: ApplicationConfiguration;

    readonly logger: winston.Logger;

    readonly model: model.Model;

    readonly mongodbClient: MongoClient;

    readonly name: string;

    readonly uuid: UUID = uuidV4();

    readonly version?: string;

    constructor(
        config: ApplicationConfiguration,
        name: string,
    ) {
        this.config = config;
        this.name = name;
        this.version = process.env.npm_package_version;

        this.logger = util.logger.build(
            config.logs,
            {
                service: this.name,
                uuid: this.uuid,
            },
        );

        this.app = util.createExpressApplication();
        this.app.use(express.json());
        this.app.use(express.text());
        const router = util.createRouter(this.logger);

        this.mongodbClient = util.createMongoDbConnection(config.services.mongodb);
        const db = this.mongodbClient.db();

        this.model = model.declare(db);

        this.app.use(router);
    }

    async start<T extends IProcessConfig>(
        processDeclaration: ProcessDeclaration<T>,
        config: T,
        name: string,
    ) {
        const {
            errors,
            config: finalConfig,
        } = processDeclaration.validateConfig(config);

        if (Array.isArray(errors) && errors.length > 0) {
            const message = errors
                .map((error) => (typeof error === 'string' ? error : error.message))
                .join('\n');
            throw new Error(message);
        }

        if (!finalConfig) {
            throw new Error('Unexpected error');
        }

        const logger = this.logger.child({ process: name });

        logger.info('Initializing process...');
        const handler = await processDeclaration.initialize(finalConfig, this, logger);
        logger.info('Process initialized');

        await this.#exposeTool('management', this.config.management, logger, handler.exposeManagementTools);
        await this.#exposeTool('documentation', this.config.documentation, logger, handler.exposeDocumentation);

        logger.info('Starting process...');
        await handler.start();
        logger.info('Process started');
    }

    /**
     * Create and register an express router.
     *
     * @param path Path of the router
     * @param logger Logger used by this router
     * @returns
     */
    createRouter(
        path: string,
        logger?: Logger,
        dynamicMeta?: DynamicMeta,
    ) : Router {
        if (path.substring(0, 1) !== '/' && path !== '') {
            throw new Error(`Path not starting by "/" character (path: ${path})`);
        }

        const router = util.createRouter(
            logger,
            dynamicMeta,
        );

        this.logger.info('Router registered', { path });
        this.app.use(path, router);
        return router;
    }

    async #exposeTool(
        name: string,
        tool: ToolConfiguration,
        logger: Logger,
        handler?: (router: Router) => void | Promise<void>,
    ) {
        if (!tool.expose || !handler) {
            return;
        }

        const logData = {
            name,
            rootPath: tool.rootPath,
        };

        const router = this.createRouter(tool.rootPath);

        util.logger.useRouter(router, logger);

        logger.info(`Exposing ${name}...`, logData);

        await handler(router);
        util.logger.useRouterError(router, logger);

        logger.info(`Exposed ${name}`, logData);
    }
}

// ============================================================
// Exports
export default Service;
