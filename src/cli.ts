import yargs from 'yargs';

function cli() {
    return yargs.usage('$0 <cmd> [args]')
        .commandDir(
            './command',
            {
                recurse: true,
                extensions: ['js', 'ts'],
            },
        )
        .help()
        .argv;
}

export default cli;
