/* eslint-env node, jest */
// ============================================================
// Import packages
import amqplibSrc from 'amqplib';
import deepmerge from 'deepmerge';
import express from 'express';
import { createLogger } from 'winston';
import faker from '@faker-js/faker';
import AsyncApiGeneratorSrc from '@asyncapi/generator';

// ============================================================
// Import modules

import initialize, {
    useConnection,
    useConnectString,
} from './initialize';
import { AmqpConnection, PartialProcessConfig, ProcessConfig } from './types';
import onEventSrc from '../../async';
import AsyncMessage from '../../AsyncMessage';
import { buildConfig } from './testHelpers';
import { createService, mock } from '../../tests/helpers';
import { randomAmqpMessage } from '../../async/testHelper';

// ============================================================
// Mocks

jest.mock('amqplib', () => {
    // eslint-disable-next-line global-require, @typescript-eslint/no-var-requires
    const { mock: mockJest } = require('../../tests/helpers');
    return mockJest.amqplib.build();
});

jest.mock('@asyncapi/generator', () => {
    const Generator = jest.fn();
    Generator.prototype.generateFromFile = jest.fn(() => Promise.resolve());

    return {
        __esModule: true,
        default: Generator,
    };
});

jest.mock('../../async', () => ({
    __esModule: true,
    default: jest.fn(() => undefined),
}));

const amqplib = amqplibSrc as unknown as mock.amqplib.Mock;
const onEvent = onEventSrc as unknown as MockedFunction<typeof onEventSrc>;
const AsyncApiGenerator = AsyncApiGeneratorSrc as unknown as MockedClass<typeof AsyncApiGeneratorSrc>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type MockedFunction<T extends (...args: any) => any> = jest.Mock<ReturnType<T>, Parameters<T>>;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type MockedClass<T extends abstract new (...args: any) => any> = jest.Mock<T, ConstructorParameters<T>>;

// ============================================================
// Tests
describe('useConnectString', () => {
    it('build the url', () => {
        const info : AmqpConnection = {
            connect: faker.internet.url(),
        };

        expect(useConnectString(info).href).toStrictEqual(`${info.connect}/`);
    });

    it('include auth if present', () => {
        const info : AmqpConnection = {
            connect: faker.internet.url(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
        };

        expect(useConnectString(info).username).toStrictEqual(info.username);
        expect(useConnectString(info).password).toStrictEqual(info.password);
    });

    it('include options if present', () => {
        const info : AmqpConnection = {
            connect: faker.internet.url(),
            frameMax: 15,
            channelMax: 10,
            heartbeat: 10,
            locale: 'fr-FR',
        };

        expect(useConnectString(info).searchParams.get('frameMax')).toStrictEqual(String(info.frameMax));
        expect(useConnectString(info).searchParams.get('channelMax')).toStrictEqual(String(info.channelMax));
        expect(useConnectString(info).searchParams.get('heartbeat')).toStrictEqual(String(info.heartbeat));
        expect(useConnectString(info).searchParams.get('locale')).toStrictEqual(String(info.locale));
    });
});

describe('useConnection', () => {
    it('connect with given information', async () => {
        const info : AmqpConnection = {
            connect: faker.internet.url(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            frameMax: 15,
            channelMax: 10,
            heartbeat: 10,
            locale: 'fr-FR',
        };

        const expectedUrl = useConnectString(info);
        await useConnection(info);

        expect(amqplib.connect).toHaveBeenCalledTimes(1);
        expect(amqplib.connect).toHaveBeenLastCalledWith(expectedUrl.href);
    });
});

describe('initialize', () => {
    beforeEach(async () => {
        const { connect } = amqplib;
        connect.mockClear();
    });

    it('initiate a connection', async () => {
        const {
            connect,
            config,
        } = await doInitialize();

        expect(connect).toHaveBeenCalledTimes(1);
        expect(connect).lastCalledWith(config.connect.connect);
    });

    it('create a channel', async () => {
        const {
            connection,
        } = await doInitialize();

        expect(connection.createChannel).toHaveBeenCalledTimes(1);
    });

    describe('start', () => {
        beforeEach(async () => {
            const { connect } = amqplib;
            connect.mockClear();
            onEvent.mockClear();
        });

        it('prefetch if configuration defined a parallel parameter', async () => {
            const {
                channel,
                config,
                process: {
                    start,
                },
            } = await doInitialize();

            await start();

            expect(channel.prefetch).toHaveBeenCalledTimes(1);
            expect(channel.prefetch).lastCalledWith(config.parallel);
        });

        it('do not prefetch if parallel parameter not defined', async () => {
            const {
                channel,
                process: {
                    start,
                },
            } = await doInitialize({
                parallel: 0,
            });

            await start();

            expect(channel.prefetch).not.toHaveBeenCalled();
        });

        it('consume the correct queue', async () => {
            const {
                channel,
                config,
                process: {
                    start,
                },
            } = await doInitialize();

            await start();

            expect(channel.consume).toHaveBeenCalledTimes(1);
            expect(channel.consume.mock.calls[0][0]).toStrictEqual(config.queue);
            expect(channel.consume.mock.calls[0][1]).toBeInstanceOf(Function);
        });
    });

    describe('consumer', () => {
        beforeEach(async () => {
            amqplib.connect.mockClear();
            onEvent.mockClear();
        });

        it('call the listener when receive message', async () => {
            const {
                consumer,
            } = await doStart();

            consumer(randomAmqpMessage());

            expect(onEvent).toHaveBeenCalledTimes(1);
        });

        it('do nothing if no message received', async () => {
            const {
                consumer,
            } = await doStart();

            consumer(null);

            expect(onEvent).not.toHaveBeenCalled();
        });

        it('call the consumer with the received content', async () => {
            const {
                consumer,
            } = await doStart();

            const event = randomAmqpMessage();

            consumer(event);
            const asyncMessage = onEvent.mock.calls[0][0];

            expect(onEvent).toHaveBeenCalledTimes(1);
            expect(asyncMessage).toBeInstanceOf(AsyncMessage);

            expect(asyncMessage.message).toBe(event);
        });

        it('create a message able to ack the channel', async () => {
            const {
                channel,
                message,
                asyncMessage,
            } = await doConsume();

            expect(channel.ack).toHaveBeenCalledTimes(0);

            asyncMessage.ack();

            expect(channel.ack).toHaveBeenCalledTimes(1);
            expect(channel.ack).toHaveBeenCalledWith(message, false);
        });

        it('create a message able to nack the channel without requeue (default)', async () => {
            const {
                channel,
                message,
                asyncMessage,
            } = await doConsume();

            expect(channel.nack).toHaveBeenCalledTimes(0);

            asyncMessage.nack();

            expect(channel.nack).toHaveBeenCalledTimes(1);
            expect(channel.nack).toHaveBeenCalledWith(message, false, false);
        });

        it('do nothing if no message received', async () => {
            const {
                consumer,
            } = await doStart();

            consumer(null);

            expect(onEvent).not.toHaveBeenCalled();
        });
    });

    describe('stop', () => {
        beforeEach(async () => {
            amqplib.connect.mockClear();
            onEvent.mockClear();
        });

        it('close both channel and connection', async () => {
            const {
                channel,
                connection,
                process,
            } = await doStart();

            await process.stop();

            expect(channel.close).toHaveBeenCalledTimes(1);
            expect(connection.close).toHaveBeenCalledTimes(1);
        });

        it('return true', async () => {
            const {
                channel,
                connection,
                process,
            } = await doStart();

            await process.stop();

            expect(channel.close).toHaveBeenCalledTimes(1);
            expect(connection.close).toHaveBeenCalledTimes(1);
        });
    });

    describe('expose documentation', () => {
        beforeEach(() => {
            AsyncApiGenerator.mockClear();
        });

        it('throw an error if "filePath" parameter not defined', async () => {
            let error: Error | undefined;

            const { process } = await doInitialize();
            const router = express.Router();

            try {
                await process.exposeDocumentation(router);
            } catch (err) {
                error = err as Error;
            }

            expect(error).not.toBeUndefined();
            expect((error as Error).message).toStrictEqual('Unexpected error');
        });

        it('throw an error if "filePath" parameter empty', async () => {
            let error: Error | undefined;

            const { process } = await doInitialize({
                asyncapi: {
                    filePath: '',
                },
            });
            const router = express.Router();

            try {
                await process.exposeDocumentation(router);
            } catch (err) {
                error = err as Error;
            }

            expect(error).not.toBeUndefined();
            expect((error as Error).message).toStrictEqual('Unexpected error');
        });

        it('generate if the right directory', async () => {
            const generationDirectory = './fake asyncApiDir';
            const filePath = './asyncapi.yml';
            const { process } = await doInitialize({
                asyncapi: {
                    filePath,
                    paths: {
                        ui: 'asyncapi',
                        yaml: 'asyncapi.yml',
                    },
                    generationDirectory,
                },
            });
            const router = express.Router();

            await process.exposeDocumentation(router);

            expect(AsyncApiGenerator).toHaveBeenCalledTimes(1);
            expect(AsyncApiGenerator).toHaveBeenCalledWith('@asyncapi/html-template', generationDirectory);

            expect(AsyncApiGenerator.prototype.generateFromFile).toHaveBeenCalledTimes(1);
            expect(AsyncApiGenerator.prototype.generateFromFile).toHaveBeenCalledWith(filePath);
        });
    });
});

async function doInitialize(extendConfig : PartialProcessConfig = {}) {
    const config = deepmerge(
        buildConfig(),
        extendConfig,
    ) as ProcessConfig;

    const result = await initialize(
        config,
        createService(),
        createLogger(),
    );

    if (amqplib.connect.mock.results[0].type !== 'return') {
        throw amqplib.connect.mock.results[0].value;
    }

    const connection = await amqplib.connect.mock.results[0].value;

    if (connection.createChannel.mock.results[0].type !== 'return') {
        throw connection.createChannel.mock.results[0].value;
    }

    const channel = await connection.createChannel.mock.results[0].value;

    return {
        connect: amqplib.connect,
        config,
        process: result,
        connection,
        channel,
    };
}

async function doStart(extendConfig : PartialProcessConfig = {}) {
    const result = await doInitialize(extendConfig);

    await result.process.start();

    const consumer = result.channel.consume.mock.calls[0][1];

    return {
        ...result,
        consumer,
    };
}

async function doConsume(extendConfig : PartialProcessConfig = {}, consumeMessage?: amqplibSrc.ConsumeMessage) {
    const result = await doStart(extendConfig);

    const message = consumeMessage || randomAmqpMessage();

    result.consumer(message);

    const asyncMessage = onEvent.mock.calls[0][0];

    return {
        ...result,
        message,
        asyncMessage,
    };
}
