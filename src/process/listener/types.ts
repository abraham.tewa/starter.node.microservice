// ============================================================
// Import modules
import { ServiceConnectionAuth, ServiceConnectionNoAuth } from '../../config/type';
import { IProcessConfig } from '../../types';

// ============================================================
// Types
interface ProcessConfig extends IProcessConfig {
    asyncapi?: AsyncApiConfiguration | AsyncApiExposed,

    connect: AmqpConnection,

    /**
     * Name of the queue to listen
     */
    queue: string,

    /**
     * A name which the server will use to distinguish message deliveries for
     * the consumer; mustn’t be already in use on the channel. It’s usually
     * easier to omit this, in which case the server will create a random
     * name and supply it in the reply.
     */
    consumerTag?: string,

    /**
     * If true, the broker won’t let anyone else consume from this queue;
     * if there already is a consumer, there goes your channel (so usually
     * only useful if you’ve made a ‘private’ queue by letting the server
     * choose its name). */
    exclusive?: boolean,

    /**
     * If true, the broker won’t expect an acknowledgement of messages
     * delivered to this consumer; i.e., it will dequeue messages as soon
     * as they’ve been sent down the wire. Defaults to false (i.e., you will be
     * expected to acknowledge messages).
     */
    noAck?: boolean,

    /**
     * Gives a priority to the consumer; higher priority consumers get messages
     * in preference to lower priority consumers. See this RabbitMQ extension’s
     * documentation
     */
    priority?: number,

    /**
     * Number of parallel message that can be treated simultaneously.
     */
    parallel: number,

    /**
     * Maximum duration of a job treatment.
     * When the time is reached, a "timeout" event is fired.
     */
    timeout: number
}

interface PartialProcessConfig extends Partial<Omit<ProcessConfig, 'asyncapi'>> {
    asyncapi?: Partial<AsyncApiConfiguration | AsyncApiExposed>
}

interface AmqpConnectionParameters {
    // See "channelMax" parameter of connect method
    // https://amqp-node.github.io/amqplib/channel_api.html#connect
    channelMax?: number,

    // See "frameMax" parameter of connect method
    // https://amqp-node.github.io/amqplib/channel_api.html#connect
    frameMax?: number,

    // See "heartbeat" parameter of connect method
    // https://amqp-node.github.io/amqplib/channel_api.html#connect
    heartbeat?: number,

    // See "locale" parameter of connect method
    // https://amqp-node.github.io/amqplib/channel_api.html#connect
    locale?: string,
}

interface AmqpConnectionNoAuth extends ServiceConnectionNoAuth, AmqpConnectionParameters {

}

interface AmqpConnectionAuth extends ServiceConnectionAuth, AmqpConnectionParameters {

}

type AmqpConnection = AmqpConnectionNoAuth | AmqpConnectionAuth;

type AsyncApiNotExposed = {
    /**
     * Absolute path to the asyncapi file.
     */
    filePath: string,
};

interface AsyncApiExposed extends AsyncApiNotExposed {
    paths: AsyncApiPaths,

    /**
     * Directory where to generate the documentation.
     */
    generationDirectory: string;
}

type AsyncApiConfiguration = AsyncApiExposed | AsyncApiNotExposed;

type AsyncApiPaths = {
    yaml: string,
    ui: string,
};

// ============================================================
// Exports
export type {
    AmqpConnection,
    AsyncApiConfiguration,
    ProcessConfig,
    PartialProcessConfig,
};
