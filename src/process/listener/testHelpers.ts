import faker from '@faker-js/faker';
import { ProcessConfig } from './types';

function buildConfig() : ProcessConfig {
    return {
        connect: {
            connect: new URL(faker.internet.url()).href,
        },
        parallel: 10,
        queue: 'some queue',
        timeout: 100,
    };
}

export {
    buildConfig,
};
