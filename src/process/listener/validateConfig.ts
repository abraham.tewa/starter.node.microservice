// ============================================================
// Import modules
import { ProcessConfigValidationResult } from '../../types';
import type {
    ProcessConfig,
} from './types';

// ============================================================
// Functions
function validateConfig(config: ProcessConfig) : ProcessConfigValidationResult<ProcessConfig> {
    const errors : string[] = [];

    return {
        errors,
        config,
    };
}

// ============================================================
// Exports
export default validateConfig;
