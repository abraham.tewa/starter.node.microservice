// ============================================================
// Import packages
import amqplib from 'amqplib';
import express, { Router } from 'express';
import AsyncApiGenerator from '@asyncapi/generator';
import { Logger } from 'winston';

// ============================================================
// Import modules
import Service from '../../Service';

import onEvent from '../../async';

import type {
    AmqpConnection,
    ProcessConfig,
} from './types';

import AsyncMessage from '../../AsyncMessage';

// ============================================================
// Functions
async function initialize(
    config: ProcessConfig,
    service: Service,
    logger: Logger,
) {
    const {
        connect,
        asyncapi,
    } = config;

    const connection = await useConnection(connect);

    const channel = await connection.createChannel();

    return {
        start: async () => {
            if (config.parallel) {
                await channel.prefetch(config.parallel);
            }

            let lastMessageId = 0;

            await channel.consume(config.queue, (msg) => {
                const receivedAt = new Date();
                if (!msg) {
                    return undefined;
                }

                lastMessageId += 1;

                const message = new AsyncMessage({
                    ackCb() {
                        channel.ack(msg, false);
                    },
                    nackCb: (requeue?: boolean) => {
                        channel.nack(msg, false, requeue);
                    },
                    message: msg,
                    logger: logger.child({
                        msgId: lastMessageId,
                    }),
                    receivedAt,
                });

                onEvent(message);

                return undefined;
            });
        },
        stop: async () => {
            await channel.close();
            await connection.close();
            return true;
        },
        async exposeDocumentation(router: Router) {
            if (!asyncapi || !('paths' in asyncapi)) {
                throw new Error('Unexpected error');
            }

            // Documentation
            const directory = asyncapi.generationDirectory as string;

            const generator = new AsyncApiGenerator(
                '@asyncapi/html-template',
                directory,
            );

            await generator.generateFromFile(asyncapi.filePath);

            router.use(
                asyncapi.paths.ui,
                express.static(directory),
            );

            router.use(
                asyncapi.paths.yaml,
                express.static(asyncapi.filePath),
            );
        },
    };
}

function useConnectString(connection: AmqpConnection) : URL {
    const {
        connect,
        channelMax,
        frameMax,
        heartbeat,
        locale,
    } = connection;

    const url = new URL(connect);

    if ('username' in connection && connection.username) {
        url.username = connection.username;
    }

    if ('password' in connection && connection.password) {
        url.password = connection.password;
    }

    if (frameMax !== undefined) {
        url.searchParams.append('frameMax', String(frameMax));
    }

    if (channelMax !== undefined) {
        url.searchParams.append('channelMax', String(channelMax));
    }

    if (heartbeat !== undefined) {
        url.searchParams.append('heartbeat', String(heartbeat));
    }

    if (locale !== undefined) {
        url.searchParams.append('locale', String(locale));
    }

    return url;
}

async function useConnection(connect : AmqpConnection) : Promise<amqplib.Connection> {
    const connectString = useConnectString(connect);

    const connection = await amqplib.connect(connectString.href);
    return connection;
}

// ============================================================
// Exports
export default initialize;
export {
    useConnectString,
    useConnection,
};
