/* eslint-env node, jest */
// ============================================================
// Import modules

import { buildConfig } from './testHelpers';
import validateConfig from './validateConfig';

describe('validateConfig', () => {
    it('return original configuration', () => {
        const buildedConfig = buildConfig();

        const { errors, config } = validateConfig(buildedConfig);

        expect(errors).toHaveLength(0);
        expect(config).toBe(buildedConfig);
    });
});
