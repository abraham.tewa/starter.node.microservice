// ============================================================
// Import modules
import serverDeclaration from './server';
import listenerDeclaration from './listener';
import { ProcessType } from '../enums';

import type {
    Configuration as ServerConfiguration,
} from './server';

// ============================================================
// Exports
export default {
    [ProcessType.listener]: listenerDeclaration,
    [ProcessType.server]: serverDeclaration,
};

export type {
    ServerConfiguration,
};
