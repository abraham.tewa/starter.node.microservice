// ============================================================
// Import packages
import swStats from 'swagger-stats';
import swaggerUi from 'swagger-ui-express';
import fs from 'fs/promises';
import YAML from 'yaml';
import net from 'net';

import * as OpenApiValidator from 'express-openapi-validator';

import type * as http from 'http';
import type {
    Express,
    Router,
} from 'express';
import type { OpenAPIV3_1 as OpenAPI } from 'openapi-types';
import type {
    Logger,
} from 'winston';

// ============================================================
// Import modules
import { OpenApiValidatorOpts } from 'express-openapi-validator/dist/openapi.validator';
import declareRoutes from '../../api';
import { InitializeProcessReturn } from '../../types';

import type Service from '../../Service';
import type { OpenApiConfiguration, ProcessConfig } from './types';

type ProcessReturn = Required<InitializeProcessReturn>;

// ============================================================
// Functions
/**
 * Start the express server process.
 *
 * The process will add several middleware that will :
 *  - Add "logger" and "requestId" property to the request
 *  - Validate the request using the openapi document
 *
 * @param processConfig
 * @returns
 */
async function initialize(
    processConfig: ProcessConfig,
    service: Service,
    logger: Logger,
) : Promise<ProcessReturn> {
    let server: http.Server;
    let reqId = 0;

    const {
        file: openApiFile,
        spec,
    } = await getOpenApiSpec(processConfig.openapi.filePath);

    const router = service.createRouter(
        '/api',
        logger,
        (req: Express.Request) => ({
            reqId: req.requestId,
        }),
    );

    router.use((req, res, next) => {
        reqId += 1;
        req.logger = logger.child({ reqId });
        req.requestId = reqId;

        next();
    });

    useOpenApiValidation(
        processConfig.openapi,
        spec,
        logger,
        router,
    );

    declareRoutes(router);

    return {
        stop: () => stopServer(server),
        start: async () => {
            const { app } = service;
            server = await startServer(
                app,
                logger,
                processConfig.port,
                processConfig.hostname,
            );

            app.request.service = service;
            app.request.model = service.model;
        },
        async exposeDocumentation(docRouter: Router) {
            if (!('paths' in processConfig.openapi) || !processConfig.openapi.paths) {
                throw new Error('Unknown error');
            }
            logger.info('Documentation paths', {
                paths: processConfig.openapi.paths,
            });

            // Exposing: openapi.json
            docRouter.get(processConfig.openapi.paths.json, (req, res) => res.json(spec));

            // Exposing: openapi.yaml
            docRouter.get(processConfig.openapi.paths.yaml, (req, res) => {
                res.contentType('application/yaml');
                res.send(openApiFile);
            });

            // Exposing: Swagger UI
            docRouter.use(processConfig.openapi.paths.ui, swaggerUi.serve, swaggerUi.setup(spec));
        },
        async exposeManagementTools(mRooter: Router) {
            if (!('paths' in processConfig.openapi) || !processConfig.openapi.paths || !processConfig.dashboard?.path) {
                throw new Error('Unknown error');
            }

            mRooter.use(
                swStats.getMiddleware({
                    name: service.name,
                    swaggerSpec: spec,
                    uriPath: processConfig.dashboard.path,
                    version: service.version,
                }),
            );

            const dashboardPath = `${processConfig.dashboard.rootPath}${processConfig.dashboard.path}`;

            logger.info(`Dashboard: http://${processConfig.hostname}:${processConfig.port}${dashboardPath}`);
        },
    };
}

/**
 * Create and start an HTTP server
 * @param app
 * @param config
 * @returns
 */
async function startServer(
    app: Express,
    logger: Logger,
    port?: number,
    hostname?: string,
): Promise<http.Server> {
    let resolve: () => void;

    const startedPromise = new Promise<void>((promiseResolve) => {
        resolve = promiseResolve;
    });

    let server: http.Server;

    const cb = () => {
        const address = server.address() as net.AddressInfo;

        const host = hostname || address.address;

        logger.info(
            `Server started: ${address.family}://${host}:${address.port}`,
            {
                hostname,
                port,
            },
        );
        resolve();
    };

    if (port && hostname) {
        server = app.listen(port, hostname, cb);
    } else if (port) {
        server = app.listen(port, cb);
    } else {
        server = app.listen(cb);
    }

    await startedPromise;

    return server;
}

/**
 * Stop a HTTP server.
 * @param server
 * @returns
 */
async function stopServer(server: http.Server) : Promise<boolean> {
    return new Promise<true>((resolve, reject) => {
        server.close((err) => {
            if (err) {
                reject(err);
            } else {
                resolve(true);
            }
        });
    });
}

async function getOpenApiSpec(
    config: string,
): Promise<{ spec: OpenAPI.Document, file: string }> {
    const file = await fs.readFile(config, 'utf8');
    const spec = YAML.parse(file) as OpenAPI.Document;

    return {
        spec,
        file,
    };
}

/**
 * Add openapi middleware validation to a router.
 *
 * @param openapi
 * @param spec
 * @param logger
 * @param router
 * @returns
 */
function useOpenApiValidation(
    openapi: OpenApiConfiguration,
    spec: OpenAPI.Document,
    logger: Logger,
    router: Router,
) {
    const { validation } = openapi;

    if (!validation) {
        return;
    }

    logger.info('Add openapi validation', { validation });

    const options : OpenApiValidatorOpts = {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        apiSpec: spec as any,
        ignorePaths: /\/_\//,
        ignoreUndocumented: false,
        validateApiSpec: true,
        validateFormats: 'full',
        validateRequests: false,
        validateResponses: false,
        validateSecurity: validation === true,
    };

    const validateResponses = validation === true || validation.response;
    const validateRequests = validation === true || validation.request;

    if (validateRequests) {
        options.validateRequests = {
            allowUnknownQueryParameters: false,
            removeAdditional: true,
        };
    }

    if (validateResponses) {
        options.validateResponses = {
            coerceTypes: false,
            removeAdditional: 'failing',
        };
    }

    const middleware = OpenApiValidator.middleware(options);
    router.use(middleware);
}

// ============================================================
// Exports
export default initialize;
export {
    getOpenApiSpec,
    stopServer,
    startServer,
    useOpenApiValidation,
};
