/* eslint-env node, jest */
// ============================================================
// Import modules
import { buildConfig } from './testHelpers';
import { ProcessConfig } from './types';
import validateConfig from './validateConfig';

// ============================================================
// Tests
describe('validate config', () => {
    it('return an empty list', () => {
        const initialConfig = buildConfig();

        const {
            config,
            errors,
        } = validateConfig(initialConfig);

        expect(config).toStrictEqual(initialConfig);
        expect(errors).toStrictEqual([]);
    });

    it('return a configuration error', () => {
        const {
            config,
            errors,
        } = validateConfig(undefined as unknown as ProcessConfig);

        expect(config).toBeUndefined();
        expect(errors).toHaveLength(1);
        expect(errors?.[0]).toBeInstanceOf(Error);
        expect((errors?.[0] as Error).message).toStrictEqual('No configuration');
    });

    it('return an error if hostname is provided without a port', () => {
        const initialConfig = buildConfig();

        initialConfig.port = undefined;

        const {
            config,
            errors,
        } = validateConfig(initialConfig);

        expect(config).toBeUndefined();
        expect(errors).toHaveLength(1);
        expect(errors?.[0]).toBeInstanceOf(Error);
        expect((errors?.[0] as Error).message).toStrictEqual('A port must be provided in order to use hostname');
    });
});
