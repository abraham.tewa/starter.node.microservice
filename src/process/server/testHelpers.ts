import deepmerge from 'deepmerge';
import { PartialProcessConfig, ProcessConfig } from './types';

function buildConfig(extendedConfig: PartialProcessConfig = {}) : ProcessConfig {
    const config = {
        dashboard: {
            path: '/dashboard',
            rootPath: '/dd',
        },
        hostname: 'some-fake-hostname.online',
        openapi: {
            filePath: './fake-openapi.json',
            paths: {
                json: '/service.json',
                ui: '/service',
                yaml: '/ayaml.yaml',
            },
            rootPath: '/root',
            validation: {
                request: true,
                response: false,
            },
        },
        port: 900,
    };

    return deepmerge(
        config,
        extendedConfig,
    ) as ProcessConfig;
}

export {
    buildConfig,
};
