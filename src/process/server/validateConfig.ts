// ============================================================
// Import modules
import { ProcessConfigValidationResult } from '../../types';
import type {
    ProcessConfig,
} from './types';

// ============================================================
// Functions
function validateConfig(config: ProcessConfig): ProcessConfigValidationResult<ProcessConfig> {
    const errors = [];

    if (!config) {
        errors.push(
            new Error('No configuration'),
        );

        return {
            errors,
            config,
        };
    }

    if (config.hostname && !config.port) {
        errors.push(
            new Error('A port must be provided in order to use hostname'),
        );
    }

    if (errors.length) {
        return {
            config: undefined,
            errors,
        };
    }

    return {
        errors,
        config,
    };
}

// ============================================================
// Exports
export default validateConfig;
