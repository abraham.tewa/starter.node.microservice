// ============================================================
// Import modules
import initialize from './initialize';
import validateConfig from './validateConfig';
import { ProcessConfig } from './types';
import { ProcessDeclaration } from '../../types';

// ============================================================
// Module constants and variables
const declaration : ProcessDeclaration<ProcessConfig> = {
    initialize,
    validateConfig,
};

// ============================================================
// Exports
export default declaration;
export type {
    ProcessConfig as Configuration,
};
