import declaration from '.';

test('is object', () => {
    expect(declaration).toHaveProperty('initialize');
    expect(declaration).toHaveProperty('validateConfig');
});
