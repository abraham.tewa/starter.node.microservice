/* eslint-env node, jest */
// ============================================================
// Import packages
import fsNotMocked from 'fs/promises';
import http from 'http';
import net from 'net';

import express, { RequestHandler, Router } from 'express';
import * as OpenApiValidatorNotMocked from 'express-openapi-validator';
import request from 'supertest';
import swStatsUnMocked, { SWStats } from 'swagger-stats';
import { createLogger } from 'winston';
import YAML from 'yaml';

import type { OpenAPIV3_1 as OpenAPI } from 'openapi-types';

// ============================================================
// Import modules
import faker from '@faker-js/faker';
import initialize, {
    getOpenApiSpec,
    startServer,
    stopServer,
    useOpenApiValidation,
} from './initialize';
import {
    buildConfig,
} from './testHelpers';
import {
    createService,
    JestTransport,
} from '../../tests/helpers';

import * as utilNotMocked from '../../util';
import {
    PartialProcessConfig,
    OpenApiConfigurationExposed,
} from './types';

// ============================================================
// Module's constants and variables

// ==========
jest.mock('fs/promises');
const fs = jest.mocked(fsNotMocked, true);

// ==========
jest.mock('express-openapi-validator', () => {
    const originalModule = jest.requireActual('express-openapi-validator');
    return {
        __esModule: true,
        ...originalModule,
        middleware: jest.fn(originalModule.middleware),
    };
});
const OpenApiValidator = jest.mocked(OpenApiValidatorNotMocked, true);

// ==========
jest.mock('swagger-stats', () => {
    const originalModule = jest.requireActual('swagger-stats');

    return {
        __esModule: true,
        ...originalModule,
        default: {
            ...originalModule.default,
            getMiddleware: jest.fn(() => {
                const middleware : RequestHandler = (req, res, next) => next();
                return middleware;
            }),
        },
    };
});

const swStats = jest.mocked(swStatsUnMocked);

// ==========
jest.mock('../../api', () => {
    const originalModule = jest.requireActual('../../api');

    return {
        __esModule: true,
        default: jest.fn((...args) => {
            originalModule.default(...args);
        }),
    };
});

// ==========
jest.mock('../../util', () => {
    const originalModule = jest.requireActual('../../util');

    return {
        __esModule: true,
        ...originalModule,
        createRouter: jest.fn((...args) => {
            const router = originalModule.createRouter(...args);

            router.get = jest.fn(router.get.bind(router));

            return router;
        }),
    };
});
const util = jest.mocked(utilNotMocked);

// ============================================================
// Tests
afterAll(() => {
    jest.clearAllTimers();
});

describe('useOpenApiValidation', () => {
    beforeEach(() => {
        OpenApiValidator.middleware.mockClear();
    });

    it('do nothing if validation not required', () => {
        const router = express.Router();
        router.use = jest.fn();

        useOpenApiValidation(
            {
                filePath: './openapi.json',
                validation: false,
            },
            buildOpenApiSpec(),
            createLogger(),
            router,
        );

        expect(router.use).not.toHaveBeenCalled();
    });

    it('use middleware when validation requested', () => {
        const router = express.Router();
        router.use = jest.fn();

        useOpenApiValidation(
            {
                filePath: './openapi.json',
                validation: {
                    request: true,
                    response: true,
                },
            },
            buildOpenApiSpec(),
            JestTransport.createLogger(),
            router,
        );

        expect(OpenApiValidator.middleware).toHaveBeenCalledTimes(1);

        const options = OpenApiValidator.middleware.mock.calls[0][0];

        expect(options.validateRequests).toStrictEqual({
            allowUnknownQueryParameters: false,
            removeAdditional: true,
        });

        expect(options.validateResponses).toStrictEqual({
            coerceTypes: false,
            removeAdditional: 'failing',
        });

        expect(options).toMatchSnapshot();
    });

    it('do not validate responses if not requested', () => {
        const router = express.Router();
        router.use = jest.fn();

        useOpenApiValidation(
            {
                filePath: './openapi.json',
                validation: {
                    request: true,
                    response: false,
                },
            },
            buildOpenApiSpec(),
            JestTransport.createLogger(),
            router,
        );

        expect(OpenApiValidator.middleware).toHaveBeenCalledTimes(1);

        const options = OpenApiValidator.middleware.mock.calls[0][0];

        expect(options.validateRequests).toStrictEqual({
            allowUnknownQueryParameters: false,
            removeAdditional: true,
        });

        expect(options.validateResponses).toBe(false);

        expect(options).toMatchSnapshot();
    });

    it('do not validate requests if not requested', () => {
        const router = express.Router();
        router.use = jest.fn();

        useOpenApiValidation(
            {
                filePath: './openapi.json',
                validation: {
                    request: false,
                    response: true,
                },
            },
            buildOpenApiSpec(),
            JestTransport.createLogger(),
            router,
        );

        expect(OpenApiValidator.middleware).toHaveBeenCalledTimes(1);

        const options = OpenApiValidator.middleware.mock.calls[0][0];

        expect(options.validateRequests).toBe(false);

        expect(options.validateResponses).toStrictEqual({
            coerceTypes: false,
            removeAdditional: 'failing',
        });

        expect(options).toMatchSnapshot();
    });
});

describe('getOpenApiSpec', () => {
    it('read and return specs', async () => {
        const filePath = './openapi.json';

        const content = YAML.stringify(buildOpenApiSpec());

        const promise = Promise.resolve(content);

        fs.readFile.mockReturnValueOnce(promise);

        const { spec, file } = await getOpenApiSpec(filePath);

        expect(file).toStrictEqual(content);
        expect(spec).toStrictEqual(YAML.parse(content));
    });
});

describe('stopServer', () => {
    it('stop the server', async () => {
        const server = new http.Server();
        const implementation = buildServerCloseMock(server);

        server.close = implementation;

        const result = await stopServer(server);

        expect(result).toBe(true);
        expect(implementation).toHaveBeenCalledTimes(1);
    });

    it('handle error correctly', async () => {
        const server = new http.Server();
        const error = new Error('fake error');
        const implementation = buildServerCloseMock(server, error);

        server.close = implementation;

        let thrownError;
        try {
            await stopServer(server);
        } catch (err) {
            thrownError = err;
        }

        expect(implementation).toHaveBeenCalledTimes(1);
        expect(thrownError).toBe(error);
    });
});

describe('[process: server] start server', () => {
    it('start the server', async () => {
        const app = express();
        const server = new http.Server();
        const port = 3200;
        const hostname = 'some.fake-domain.online';

        server.address = () : net.AddressInfo => ({
            address: '127.0.0.1',
            family: 'IPv4',
            port,
        });

        const listen = jest.fn(buildServerListenMock(server));
        app.listen = listen as unknown as express.Application['listen'];
        const loggerSpy = jest.fn();

        const result = await startServer(
            app,
            JestTransport.createLogger(loggerSpy),
            port,
            hostname,
        );

        expect(result).toBeInstanceOf(http.Server);
        expect(listen).toHaveBeenCalledTimes(1);
        expect(listen.mock.calls[0][0]).toStrictEqual(port);
        expect(listen.mock.calls[0][1]).toStrictEqual(hostname);
        expect(loggerSpy).toMatchSnapshot();
    });

    it('start the server even if no hostname nor port is defined', async () => {
        const app = express();

        const server = await startServer(
            app,
            JestTransport.createLogger(),
        );

        expect(server.address()).not.toBeUndefined();

        await new Promise((resolve, reject) => {
            server.close((err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(err);
                }
            });
        });
    });

    it('start the server even if no hostname is defined', async () => {
        const app = express();

        const server = await startServer(
            app,
            JestTransport.createLogger(),
            faker.internet.port(),
        );

        expect(server.address()).not.toBeUndefined();
        await new Promise((resolve, reject) => {
            server.close((err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(err);
                }
            });
        });
    });
});

describe('[process: server] initialize', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('initialize', async () => {
        const config = buildConfig();

        const { filePath } = config.openapi;

        const content = YAML.stringify(buildOpenApiSpec());

        const promise = Promise.resolve(content);

        fs.readFile.mockReturnValueOnce(promise);

        await initialize(
            config,
            createService(),
            JestTransport.createLogger(),
        );

        expect(fs.readFile).toHaveBeenCalledTimes(1);
        expect(fs.readFile).toHaveBeenCalledWith(filePath, 'utf8');
    });

    it('add an id middleware that increment at each call', async () => {
        const {
            router,
            spy,
            service,
        } = await doInitialize();

        const responseText = 'some response';
        const path = '/path';

        const getSpy = jest.fn((req, res) => {
            res.send(responseText);
        });

        router.get(path, getSpy);

        const checkRequestID = async (callIndex: number) => {
            getSpy.mockClear();

            const res = await request(service.app)
                .get(`/api/${path}`);

            expect(res.status).toStrictEqual(200);
            expect(getSpy).toHaveBeenCalledTimes(1);

            const [req] = getSpy.mock.calls[0];

            expect(req).toHaveProperty('requestId', callIndex + 1);

            const lastLoggerCall = spy.logger.mock.calls[spy.logger.mock.calls.length - 1][0];
            const { meta } = lastLoggerCall;

            delete meta.req;
            delete meta.responseTime;

            expect(meta).toMatchSnapshot();
        };

        await checkRequestID(0);
        await checkRequestID(1);
    });
});

describe('[process: server] process.start', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('start the server', async () => {
        const {
            config,
            service,
            start,
        } = await doInitialize();

        const server = new http.Server();

        server.address = () : net.AddressInfo => ({
            address: '127.0.0.1',
            family: 'IPv4',
            port: config.port as number,
        });

        const listenMock = jest.fn((...args) => {
            const cb = args[args.length - 1];
            setTimeout(() => cb());

            return server;
        });

        service.app.listen = listenMock as unknown as typeof service.app.listen;

        await start();

        expect(listenMock).toHaveBeenCalledTimes(1);
        expect(listenMock.mock.calls[0][0]).toBe(config.port);
        expect(listenMock.mock.calls[0][1]).toBe(config.hostname);
    });
});

describe('[process: server] process.stop', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('stop the server', async () => {
        const {
            service,
            start,
            stop,
        } = await doInitialize();

        // Creating server mock
        const server = {
            close: jest.fn((cb) => {
                cb();
            }),

            address() : net.AddressInfo {
                return {
                    address: '127.0.0.1',
                    family: 'IPv4',
                    port: 900,
                };
            },
        };

        // The app.listen will return the server mock
        const listenMock = jest.fn((...args) => {
            const cb = args[args.length - 1];
            setTimeout(() => cb());

            return server;
        });

        service.app.listen = listenMock as unknown as typeof service.app.listen;

        await start();

        await stop(0);

        expect(server.close).toHaveBeenCalledTimes(1);
    });

    it('Handle correctly error', async () => {
        const {
            service,
            start,
            stop,
        } = await doInitialize();

        const fakeError = new Error('fake error');

        // Creating server mock
        const server = {
            close: jest.fn((cb) => {
                cb(fakeError);
            }),

            address() : net.AddressInfo {
                return {
                    address: '127.0.0.1',
                    family: 'IPv4',
                    port: 900,
                };
            },
        };

        // The app.listen will return the server mock
        const listenMock = jest.fn((...args) => {
            const cb = args[args.length - 1];
            setTimeout(() => cb(), 0);

            return server;
        });

        service.app.listen = listenMock as unknown as typeof service.app.listen;

        await start();

        let error;

        try {
            await stop(0);
        } catch (err) {
            error = err;
        }

        expect(server.close).toHaveBeenCalledTimes(1);
        expect(error).toBe(fakeError);
    });
});

describe('[process: server] process.exposeDocumentation', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('Ensure valid properties', async () => {
        const {
            exposeDocumentation,
            router,
        } = await doInitialize({
            openapi: {
                paths: undefined,
            },
        });

        let error: Error | undefined;

        try {
            await exposeDocumentation?.(router);
        } catch (err) {
            error = err as Error;
        }

        expect(error).toBeInstanceOf(Error);
        expect(error?.message).toEqual('Unknown error');
    });

    it('return json spec', async () => {
        const app = express();
        const router = express.Router();

        app.use('/doc', router);

        const getSpy = jest.fn(router.get.bind(router));
        router.get = getSpy as unknown as typeof router.get;

        const {
            config,
            exposeDocumentation,
            spec,
        } = await doInitialize();

        await exposeDocumentation?.(router);

        const { paths } = config.openapi as OpenApiConfigurationExposed;

        const response = await request(app).get(`/doc/${paths.json}`);

        expect(response.status).toBe(200);
        expect(response.body).toEqual(spec);
    });

    it('return yaml spec', async () => {
        const app = express();
        const router = express.Router();

        app.use('/doc', router);

        const getSpy = jest.fn(router.get.bind(router));
        router.get = getSpy as unknown as typeof router.get;

        const {
            config,
            exposeDocumentation,
            specFile,
        } = await doInitialize();

        await exposeDocumentation?.(router);

        const { paths } = config.openapi as OpenApiConfigurationExposed;

        const response = await request(app).get(`/doc/${paths.yaml}`);

        expect(response.status).toBe(200);
        expect(response.headers['content-type']).toMatch(/^application\/yaml/);
        expect(response.text).toStrictEqual(specFile);
    });
});

describe('[process: server] process.exposeManagementTools', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('Ensure valid properties (paths)', async () => {
        const {
            exposeManagementTools,
            router,
        } = await doInitialize({
            openapi: {
                paths: undefined,
            },
        });

        let error: Error | undefined;

        try {
            await exposeManagementTools?.(router);
        } catch (err) {
            error = err as Error;
        }

        expect(error).toBeInstanceOf(Error);
        expect(error?.message).toEqual('Unknown error');
    });

    it('Ensure valid properties (dashboard)', async () => {
        const {
            exposeManagementTools,
            router,
        } = await doInitialize({
            dashboard: {
                path: undefined,
            },
            openapi: {
                paths: undefined,
            },
        });

        let error: Error | undefined;

        try {
            await exposeManagementTools?.(router);
        } catch (err) {
            error = err as Error;
        }

        expect(error).toBeInstanceOf(Error);
        expect(error?.message).toEqual('Unknown error');
    });

    it('add middleware', async () => {
        const app = express();
        const router = express.Router();

        app.use('/tool', router);

        const getSpy = jest.fn(router.get.bind(router));
        router.get = getSpy as unknown as typeof router.get;

        const {
            config,
            exposeManagementTools,
            service,
        } = await doInitialize();

        await exposeManagementTools?.(router);

        expect(swStats.getMiddleware).toHaveBeenCalledTimes(1);

        const swOptions = swStats.getMiddleware.mock.calls[0][0] as SWStats;
        expect(swOptions.name).toBe(service.name);
        expect(swOptions.version).toBe(service.version);
        expect(swOptions.uriPath).toBe(config.dashboard?.path);
    });
});

// ============================================================
// Helpers
function buildOpenApiSpec() : OpenAPI.Document {
    return {
        openapi: '3.0.0',
        info: {
            license: {
                name: 'MIT',
            },
            title: 'fake title',
            version: '0.0.0',
        },
        paths: {
            '/status': {
                get: {
                    responses: {
                        200: {
                            description: 'Fake description',
                        },
                    },
                },
            },
        },
    };
}

function buildServerCloseMock(server: http.Server, error?: Error) {
    const implementation = (cb?: (err?: Error) => void) => {
        if (error) {
            cb?.(error);
        } else {
            cb?.();
        }

        return server;
    };

    return jest.fn(implementation);
}

function buildServerListenMock(server: http.Server): express.Application['listen'] {
    return (...args: unknown[]) => {
        const cb = args[args.length - 1];

        if (typeof cb === 'function') {
            setTimeout(() => cb(), 0);
        }

        return server;
    };
}

async function doInitialize(
    extendedConfig: PartialProcessConfig = {
        openapi: {
            validation: false,
        },
    },
) {
    const loggerSpy = jest.fn();

    const config = buildConfig(extendedConfig);

    const spec = buildOpenApiSpec();
    const specFile = YAML.stringify(spec);

    const promise = Promise.resolve(specFile);

    const service = createService(loggerSpy);
    const { app } = service;

    fs.readFile.mockReturnValueOnce(promise);

    const result = await initialize(
        config,
        service,
        service.logger,
    );

    expect(util.createRouter).toHaveBeenCalledTimes(2);
    const router = util.createRouter.mock.results[1].value as Router;

    return {
        app,
        config,
        router,
        service,
        spec,
        specFile,
        spy: {
            logger: loggerSpy,
        },
        ...result,
    };
}
