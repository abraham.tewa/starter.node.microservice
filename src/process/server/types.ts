// ============================================================
// Import modules
import { IProcessConfig } from '../../types';

// ============================================================
// Types
interface ProcessConfig extends IProcessConfig {
    // If a string is provided, it will be the URL of the dashboard
    dashboard?: {
        path: string,
        rootPath: string
    },
    openapi: OpenApiConfiguration,
    hostname?: string,
    port?: number,
}

interface PartialProcessConfig extends IProcessConfig {
    dashboard?: Partial<ProcessConfig['dashboard']>,
    openapi?: Partial<OpenApiConfiguration>,
}

type OpenApiConfiguration = OpenApiConfigurationExposed | OpenApiConfigurationNotExposed;

type OpenApiConfigurationNotExposed = {
    filePath: string,
    validation: OpenApiValidation | boolean,
};

interface OpenApiConfigurationExposed extends OpenApiConfigurationNotExposed {
    paths: OpenApiUrls,
    rootPath: string,
}

type OpenApiValidation = {
    request: boolean,
    response: boolean,
};

type OpenApiUrls = {
    // Path to the JSON open api file
    // The file will be exposed through the documentation router
    // Ex: openapi.json
    json: string,

    // Path to the swagger UI interface
    // The file will be exposed through the documentation router
    // Ex: /openapi
    ui: string,

    // Path to the YAML open api file
    // The file will be exposed through the documentation router
    // Ex: _/openapi.yml
    yaml: string,
};

// ============================================================
// Exports
export type {
    OpenApiConfiguration,
    OpenApiConfigurationExposed,
    ProcessConfig,
    PartialProcessConfig,
};
