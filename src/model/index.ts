// ============================================================
// Import modules
import * as mongodb from 'mongodb';

import * as users from './users';

// ============================================================
// Functions
function declare(database: mongodb.Db) {
    const userCollection = database.collection<users.Document>('user');

    const model = {
        users: users.declare(userCollection),
    };

    return model;
}

// ============================================================
// Types
type Model = ReturnType<typeof declare>;

// ============================================================
// Exports
export {
    declare,
};

export type {
    Model,
};
