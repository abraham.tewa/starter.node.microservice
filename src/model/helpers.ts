// ============================================================
// Functions
function toAppObject<T extends AppObject>(dbObject: DbObject<T>): T {
    const {
        _id: id,
        ...appObject
    } = dbObject;

    return {
        ...(appObject as T),
        id: dbObject._id.toHexString(),
    };
}

// ============================================================
// Exports
export {
    toAppObject,
};
