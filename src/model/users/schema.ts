// ============================================================
// Import packages
import Joi from 'joi';
import type * as mongodb from 'mongodb';

// ============================================================
// Schema

/**
 * Represent the schema of the object
 * stored in the database.
 */
const schema = Joi.object<Type>({
    id: Joi.number()
        .required(),
    username: Joi.string()
        .max(64)
        .required(),
    createdAt: Joi.date().required(),
    lastModifiedAt: Joi.date().required(),
});

/**
 * Represent the schema of the object
 * to pass for update.
 */
const update = Joi.object<Update>({
    id: Joi.number()
        .required(),
    username: Joi.string()
        .max(64)
        .required(),
});

/**
 * Represent the schema of the object to pass for insert
 */
const insert = Joi.object<Insert>({
    username: Joi.string()
        .max(64)
        .required(),
});

/**
 * Represent the schema of the object to pass for upsert
 */
const upsert = Joi.alternatives(update, insert) as Joi.Schema<Upsert>;

// ============================================================
// Types
type Document = mongodb.WithId<Omit<Type, 'id'>>;

type Type = {
    id: string,
    username: string,
    createdAt: Date,
    lastModifiedAt: Date,
};

interface Update extends Partial<Omit<Type, 'createdAt' | 'lastModifiedAt'>> {
    id: Type['id'],
}

type Insert = Omit<Type, 'id' | 'createdAt' | 'lastModifiedAt'>;

type Upsert = Update | Update;

// ============================================================
// Exports
export default schema;

export {
    schema,

    insert,
    update,
    upsert,
};

export type {
    Type,
    Document,

    Insert,
    Upsert,
    Update,
};
