export { default as declare } from './model';
export type {
    Document,
    Type,
} from './schema';
