// ============================================================
// Imports
import { Collection, ObjectId } from 'mongodb';
import { toAppObject } from '../helpers';
import * as schema from './schema';

// ============================================================
// Functions

async function create(
    collection: Collection<schema.Document>,
    user: schema.Insert,
): Promise<schema.Type> {
    await schema.insert.validateAsync(user);

    const date = new Date();

    const created = await collection.insertOne({
        ...user,
        createdAt: date,
        lastModifiedAt: date,
    } as schema.Document);

    return {
        id: created.insertedId.toHexString(),
        ...user,
        lastModifiedAt: date,
        createdAt: date,
    };
}

async function update(
    collection: Collection<schema.Document>,
    user: schema.Update,
): Promise<schema.Type | null> {
    await schema.update.validateAsync(user);

    const date = new Date();

    const created = await collection.findOneAndUpdate(
        { _id: new ObjectId(user.id) },
        {
            ...user,
            lastModifiedAt: date,
        },
    );

    if (!created.value) {
        return null;
    }

    return toAppObject<schema.Type>(created.value);
}

async function findById(
    collection: Collection<schema.Document>,
    id: string,
): Promise<schema.Type | null> {
    const find = await collection.findOne({
        _id: new ObjectId(id),
    });

    if (!find) {
        return null;
    }

    return toAppObject<schema.Type>(find);
}

async function findByUsername(
    collection: Collection<schema.Document>,
    username: string,
): Promise<schema.Type | null> {
    const find = await collection.findOne({
        username,
    });

    if (!find) {
        return null;
    }

    return toAppObject<schema.Type>(find);
}

function declare(collection: Collection<schema.Document>) {
    return {
        findById: findById.bind(undefined, collection),
        findByUsername: findByUsername.bind(undefined, collection),
        create: create.bind(undefined, collection),
        update: update.bind(undefined, collection),
        schema,
    };
}

// ============================================================
// Exports
export default declare;
