// ============================================================
// Import packages
import express from 'express';
import expressWinston from 'express-winston';

import type { Logger } from 'winston';
import type { Router } from 'express';

// ============================================================
// Import functions
function createRouter(
    logger?: Logger,
    dynamicMeta?: (
        req: express.Request,
        res: express.Response,
        err: Error,
    ) => object,
): Router {
    const router = express.Router();

    if (logger) {
        router.use(
            expressWinston.logger({
                winstonInstance: logger,
                dynamicMeta,
                // optional: control whether you want to log the meta data about the request (default to true)
                meta: true,
                msg: 'HTTP {{req.method}} {{req.url}}',
                expressFormat: true,
                colorize: false,
            }),
        );
    }

    return router;
}

// ============================================================
// Exports
export default createRouter;
