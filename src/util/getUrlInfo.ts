import { CONNECT_STRING } from '../constants';

function getUrlInfo(url: string, defaultPort?: number): ConnectStringInfo | undefined {
    if (!url) {
        return undefined;
    }

    const info = CONNECT_STRING.exec(url);

    if (!info) {
        return undefined;
    }

    const [
        ,
        protocol,
        auth,
        hosts,
        path,
        params,
    ] = info;

    const AUTH_REGEX = /(.+):(.+)/;

    let username: string | undefined;
    let password: string | undefined;

    const authInfo = AUTH_REGEX.exec(auth);

    if (authInfo) {
        [, username, password] = authInfo;
    }

    return {
        protocol,
        auth,
        username,
        password,
        hosts: hosts
            .split(',')
            .map((hostPort) => {
                if (!isValidHostPort(hostPort, defaultPort)) {
                    throw new Error(`Invalid host/port: ${hostPort}`);
                }

                const colonIndex = hostPort.indexOf(':');
                const host = hostPort.substring(0, colonIndex);
                const port = hostPort.substring(colonIndex + 1);

                return {
                    host,
                    port: Number(port),
                };
            }),
        path: path ? `/${path}` : '',
        params: new URLSearchParams(params),
    };
}

function isValidHostPort(hostPort: string, defaultPort?: number): boolean {
    let url;

    if (defaultPort !== undefined && !isValidPort(defaultPort)) {
        throw new Error(`Invalid default port: ${defaultPort}`);
    }

    try {
        url = new URL(`http://${hostPort}`);
    } catch (err) {
        return false;
    }

    if (!url.port && defaultPort) {
        return true;
    }

    if (!url.port) {
        return false;
    }

    const port = Number(url.port);

    return isValidPort(port);
}

function isValidPort(port: number) : boolean {
    return Number.isInteger(port) && port >= 0 && port <= 65_535;
}

type ConnectStringInfo = {
    protocol: string,
    auth?: string,
    username?: string,
    password?: string,
    hosts: Array<{ host: string, port?: number }>,
    path: string,
    params: URLSearchParams,
};

export default getUrlInfo;
export {
    isValidHostPort,
};
export type {
    ConnectStringInfo,
};
