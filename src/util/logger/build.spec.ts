/* eslint-env node, jest */
// ============================================================
// Import packages
import winston from 'winston';

// ============================================================
// Import modules
import build, { buildLoggerOptions } from './build';
import { LogFormat, LogLevel, LogType } from '../../enums';

import type {
    LogConfiguration,
} from './build';
import prettyFormat from './prettyFormat';
import { JestTransport } from '../../tests/helpers';

jest.mock('./prettyFormat');

// ============================================================
// Tests
describe('build', () => {
    it('build JSON logger', () => {
        const config : LogConfiguration = {
            format: LogFormat.json,
            level: LogLevel.info,
            transports: [],
        };

        expect(() => build(config, {})).not.toThrowError();
    });

    it('build text logger', () => {
        const config : LogConfiguration = {
            format: LogFormat.text,
            level: LogLevel.info,
            transports: [],
        };

        expect(() => build(config, {})).not.toThrowError();
    });

    it('build pretty logger', () => {
        const config : LogConfiguration = {
            format: LogFormat.pretty,
            level: LogLevel.info,
            transports: [],
        };

        expect(() => build(config, {})).not.toThrowError();
    });

    it('call pretty formatter', async () => {
        const config : LogConfiguration = {
            format: LogFormat.pretty,
            level: LogLevel.info,
            transports: [
                new JestTransport(),
            ],
        };

        const logger = build(config, {});

        logger.info('test');

        expect(prettyFormat).toHaveBeenCalledTimes(1);
    });
});

describe('buildOptions', () => {
    it('return Console transport', () => {
        const config : LogConfiguration = {
            format: LogFormat.json,
            level: LogLevel.info,
            transports: [
                {
                    type: LogType.console,
                },
            ],
        };

        const options = buildLoggerOptions(config, {});

        expect(options.transports).toBeInstanceOf(Array);
        if (Array.isArray(options.transports)) {
            expect(options?.transports?.[0]).toBeInstanceOf(winston.transports.Console);
        }
    });

    it('return Http transport', () => {
        const config : LogConfiguration = {
            format: LogFormat.json,
            level: LogLevel.info,
            transports: [
                {
                    type: LogType.http,
                },
            ],
        };

        const options = buildLoggerOptions(config, {});

        expect(options.transports).toBeInstanceOf(Array);
        if (Array.isArray(options.transports)) {
            expect(options?.transports?.[0]).toBeInstanceOf(winston.transports.Http);
        }
    });

    it('return File transport', () => {
        const config : LogConfiguration = {
            format: LogFormat.json,
            level: LogLevel.info,
            transports: [
                {
                    type: LogType.file,
                    filename: 'string',
                },
            ],
        };

        const options = buildLoggerOptions(config, {});

        expect(options.transports).toBeInstanceOf(Array);
        if (Array.isArray(options.transports)) {
            expect(options?.transports?.[0]).toBeInstanceOf(winston.transports.File);
        }
    });
});
