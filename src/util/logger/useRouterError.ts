// ============================================================
// Import packages
import expressWinston from 'express-winston';
import { Router } from 'express';

import type {
    Logger,
} from 'winston';

// ============================================================
// Function
function useRouterError(router: Router, parentLogger: Logger) {
    router.use(
        expressWinston.errorLogger({
            winstonInstance: parentLogger,
        }),
    );
}

// ============================================================
// Exports
export default useRouterError;
