/* eslint-env node, jest */
// ============================================================
// Import packages
import { v4 as UUID } from 'uuid';

// ============================================================
// Import modules
import { LogLevel } from '../../enums';
import prettyFormat from './prettyFormat';

// ============================================================
// Tests

describe('prettyFormat', () => {
    it('with extra data', () => {
        const time = '00:00:00';
        const level = LogLevel.verbose;
        const message = 'some random message, not very helpful';
        const service = 'current-service';
        const process = 'some-process';
        const timestamp = `2022-01-01T${time}.00Z`;
        const uuid = UUID();

        const extraData = {
            key: 'some value',
        };

        const info = {
            extraData,
            level,
            message,
            process,
            service,
            timestamp,
            uuid,
        };

        const log = prettyFormat(info);

        expect(log).toEqual(expect.stringContaining(level.toUpperCase()));
        expect(log).toEqual(expect.stringContaining(message));
        expect(log).toEqual(expect.stringContaining(extraData.key));
        expect(log).toEqual(expect.stringContaining(process));
        expect(log).toEqual(expect.not.stringContaining(service));
        expect(log).toEqual(expect.not.stringContaining(timestamp));
        expect(log).toEqual(expect.stringContaining(time));
        expect(log).toEqual(expect.not.stringContaining(uuid));

        expect(log).toMatchSnapshot();
    });

    it('without extra data', () => {
        const time = '00:00:00';
        const level = LogLevel.verbose;
        const message = 'some random message, not very helpful';
        const service = 'current-service';
        const process = 'some-process';
        const timestamp = `2022-01-01T${time}.00Z`;
        const uuid = UUID();

        const info = {
            level,
            message,
            process,
            service,
            timestamp,
            uuid,
        };

        const log = prettyFormat(info);

        expect(log).toEqual(expect.stringContaining(level.toUpperCase()));
        expect(log).toEqual(expect.stringContaining(message));
        expect(log).toEqual(expect.stringContaining(process));
        expect(log).toEqual(expect.not.stringContaining(service));
        expect(log).toEqual(expect.not.stringContaining(timestamp));
        expect(log).toEqual(expect.stringContaining(time));
        expect(log).toEqual(expect.not.stringContaining(uuid));

        expect(log).toMatchSnapshot();
    });

    it('without process', () => {
        const time = '00:00:00';
        const level = LogLevel.verbose;
        const message = 'some random message, not very helpful';
        const service = 'current-service';
        const timestamp = `2022-01-01T${time}.00Z`;
        const uuid = UUID();

        const info = {
            level,
            message,
            service,
            timestamp,
            uuid,
        };

        const log = prettyFormat(info);

        expect(log).toEqual(expect.stringContaining(level.toUpperCase()));
        expect(log).toEqual(expect.stringContaining(message));
        expect(log).toEqual(expect.not.stringContaining(service));
        expect(log).toEqual(expect.not.stringContaining(timestamp));
        expect(log).toEqual(expect.stringContaining(time));
        expect(log).toEqual(expect.not.stringContaining(uuid));

        expect(log).toMatchSnapshot();
    });
});
