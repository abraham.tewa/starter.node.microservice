// ============================================================
// Import packages
import expressWinston from 'express-winston';
import { Router } from 'express';
import type { Logger } from 'winston';

// ============================================================
// Function
function useRouter(router: Router, parentLogger: Logger) {
    router.use(
        expressWinston.logger({
            winstonInstance: parentLogger,
            meta: true, // optional: control whether you want to log the meta data about the request (default to true)
            msg: 'HTTP {{req.method}} {{req.url}}',
            expressFormat: true,
            colorize: false,
        }),
    );
}

// ============================================================
// Exports
export default useRouter;
