// ============================================================
// Import packages
import chalk, { ChalkFunction } from 'chalk';
import winston from 'winston';

// ============================================================
// Import modules
import { LogLevel } from '../../enums';

// ============================================================
// Module's constants and variables
const LevelColorMaps : LevelColorFunction = {
    [LogLevel.critical]: chalk.red,
    [LogLevel.error]: chalk.red,
    [LogLevel.warn]: chalk.yellow,
    [LogLevel.info]: chalk.blue,
    [LogLevel.verbose]: chalk.green,
    [LogLevel.trace]: chalk.white,
    [LogLevel.debug]: chalk.bold.red,
};

// ============================================================
// Functions
function prettyFormat(info: LogInfo) {
    const formattedDate = chalk.green(info.timestamp.substring(11, 19));
    const level = LevelColorMaps[info.level as LogLevel](info.level.toUpperCase());
    const { process } = info;

    const obj = { ...info } as Partial<winston.Logform.TransformableInfo>;
    delete obj.level;
    delete obj.message;
    delete obj.service;
    delete obj.process;
    delete obj.timestamp;
    delete obj.uuid;

    const strData = Object.keys(obj).length
        ? chalk.gray(`\n${JSON.stringify(obj, undefined, 2)}`)
        : '';

    const processStr = process ? `${chalk.gray(process)} ` : '';

    return `[${level}] ${formattedDate} ${processStr}${info.message}${strData}`;
}

// ============================================================
// Types

type LevelColorFunction = {
    [key in LogLevel]: ChalkFunction
};

type LogInfo = {
    level: string,
    message: string,
    process?: string,
    timestamp: string,
};

// ============================================================
// Exports
export default prettyFormat;
export type {
    LogInfo,
};
