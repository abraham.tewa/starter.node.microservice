// ============================================================
// Import packages
import winston from 'winston';
import WinstonTransport from 'winston-transport';

// ============================================================
// Import modules

import prettyFormat, { LogInfo } from './prettyFormat';

import {
    LogFormat,
    LogLevel,
    LogType,
} from '../../enums';

import {
    ConsoleTransportOptions,
    FileTransportOptions,
    HttpTransportOptions,
} from '../../config/type';

// ============================================================
// Module's constants and variables

const LogTransportMap : LogTransportMapRecord = {
    [LogType.console]: (option) => new winston.transports.Console(option),
    [LogType.file]: (option) => new winston.transports.File(option),
    [LogType.http]: (option) => new winston.transports.Http(option),
};

const LogFormatMap : LogFormatMapRecord = {
    [LogFormat.json]: () => winston.format.json(),
    [LogFormat.pretty]: () => winston.format.printf((info) => prettyFormat(info as LogInfo)),
    [LogFormat.text]: () => winston.format.simple(),
};

const ListColorLevels : LevelColorMapsType = {
    [LogLevel.critical]: 0,
    [LogLevel.error]: 1,
    [LogLevel.warn]: 2,
    [LogLevel.info]: 3,
    [LogLevel.verbose]: 4,
    [LogLevel.trace]: 5,
    [LogLevel.debug]: 6,
};

// ============================================================
// Functions

/**
 * Build the service logger
 * @param config
 * @returns
 */
function build(
    config: LogConfiguration,
    defaultMeta: object,
) : winston.Logger {
    const options = buildLoggerOptions(config, defaultMeta);
    const logger = winston.createLogger(options);

    return logger;
}

function buildLoggerOptions(
    config: LogConfiguration,
    defaultMeta: object,
) : winston.LoggerOptions {
    const transports = config.transports.map((info) => {
        if (info instanceof WinstonTransport) {
            return info;
        }

        const { type, ...option } = info;
        return LogTransportMap[type](option);
    });

    const format = LogFormatMap[config.format]();

    return {
        levels: ListColorLevels,
        format: winston.format.combine(
            winston.format.timestamp(),
            format,
        ),
        transports,
        defaultMeta,
    };
}

// ============================================================
// Types
type LogFormatMapRecord = Record<LogFormat, () => winston.Logform.Format>;
type LogTransportMapRecord = Record<LogType, (option: object) => WinstonTransport>;

type LogConfiguration = {
    level: LogLevel,
    format: LogFormat,
    transports: Array<FileTransportOptions | ConsoleTransportOptions | HttpTransportOptions | WinstonTransport>,
};

type LevelColorMapsType = {
    [key in LogLevel]: number
};

// ============================================================
// Exports
export default build;
export {
    buildLoggerOptions,
};
export type {
    LogConfiguration,
};
