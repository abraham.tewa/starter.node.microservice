/* eslint-env node, jest */
// ============================================================
// Import modules
import getUrlInfo, { ConnectStringInfo, isValidHostPort } from './getUrlInfo';

// ============================================================
// Functions

describe('getUrlInfo', () => {
    it('return undefined if URL is not valid', () => {
        expect(getUrlInfo('')).toBeUndefined();
        expect(getUrlInfo('http//')).toBeUndefined();
    });

    it('simple url', () => {
        const info = getUrlInfo('mongodb://localhost:27017');
        const expectedInfo : ConnectStringInfo = {
            protocol: 'mongodb',
            auth: undefined,
            hosts: [
                { host: 'localhost', port: 27017 },
            ],
            path: '',
            params: new URLSearchParams(),
            password: undefined,
            username: undefined,
        };

        expect(info).toStrictEqual(expectedInfo);
    });

    it('multiple hosts', () => {
        const info = getUrlInfo('mongodb://sub1.domain:27017,sub2.domain:27018');
        const expectedInfo : ConnectStringInfo = {
            protocol: 'mongodb',
            auth: undefined,
            hosts: [
                { host: 'sub1.domain', port: 27017 },
                { host: 'sub2.domain', port: 27018 },
            ],
            path: '',
            params: new URLSearchParams(),
            password: undefined,
            username: undefined,
        };

        expect(info).toStrictEqual(expectedInfo);
    });

    it('with auth', () => {
        const info = getUrlInfo('mongodb://guest:password@sub1.domain:27017,sub2.domain:27018');
        const expectedInfo : ConnectStringInfo = {
            protocol: 'mongodb',
            auth: 'guest:password',
            hosts: [
                { host: 'sub1.domain', port: 27017 },
                { host: 'sub2.domain', port: 27018 },
            ],
            path: '',
            params: new URLSearchParams(),
            password: 'password',
            username: 'guest',
        };

        expect(info).toStrictEqual(expectedInfo);
    });

    it('with path', () => {
        const info = getUrlInfo('mongodb://guest:password@sub1.domain:27017,sub2.domain:27018/one/path');
        const expectedInfo : ConnectStringInfo = {
            protocol: 'mongodb',
            auth: 'guest:password',
            hosts: [
                { host: 'sub1.domain', port: 27017 },
                { host: 'sub2.domain', port: 27018 },
            ],
            path: '/one/path',
            params: new URLSearchParams(),
            password: 'password',
            username: 'guest',
        };

        expect(info).toStrictEqual(expectedInfo);
    });

    it('with params', () => {
        const url = 'mongodb://guest:password@sub1.domain:27017,sub2.domain:27018/one/path?a=b&c=d';
        const { params, ...info } = getUrlInfo(url) as ConnectStringInfo;

        const expectedInfo : Omit<ConnectStringInfo, 'params'> = {
            protocol: 'mongodb',
            auth: 'guest:password',
            hosts: [
                { host: 'sub1.domain', port: 27017 },
                { host: 'sub2.domain', port: 27018 },
            ],
            path: '/one/path',
            password: 'password',
            username: 'guest',
        };

        const expectedParams = new URLSearchParams();
        expectedParams.append('a', 'b');
        expectedParams.append('c', 'd');

        expect(info).toStrictEqual(expectedInfo);
        expect(params?.toString()).toStrictEqual(expectedParams.toString());
    });

    it('throw an error if domain/port is not valid', () => {
        const url = 'mongodb://localhost:99999';
        expect(() => getUrlInfo(url)).toThrowError('Invalid host/port: localhost:99999');
    });
});

describe('isValidHostPort', () => {
    it('validate simple case', () => {
        expect(isValidHostPort('localhost:123')).toStrictEqual(true);
        expect(isValidHostPort('sub.domain:123')).toStrictEqual(true);
        expect(isValidHostPort('def.sub.domain:123')).toStrictEqual(true);
    });

    it('invalid invalid ports', () => {
        expect(isValidHostPort('localhost')).toStrictEqual(false);
        expect(isValidHostPort('localhost:aa')).toStrictEqual(false);
        expect(isValidHostPort('localhost:11a22')).toStrictEqual(false);
        expect(isValidHostPort('localhost:-1')).toStrictEqual(false);
        expect(isValidHostPort('localhost:999999')).toStrictEqual(false);
        expect(isValidHostPort('localhost:9999.99')).toStrictEqual(false);
    });

    it('invalid invalid domain', () => {
        expect(isValidHostPort('sub#domain:123')).toStrictEqual(false);
    });

    it('allow default port', () => {
        expect(isValidHostPort('localhost', 80)).toStrictEqual(true);
    });

    it('throw an exception if the default port is not valid', () => {
        expect(() => isValidHostPort('localhost', 99999)).toThrowError('Invalid default port: 99999');
    });
});
