// ============================================================
// Import packages
import express from 'express';
import helmet from 'helmet';

// ============================================================
// Functions
function createExpressApplication() {
    const app = express();

    app.use(helmet());

    return app;
}

// ============================================================
// Exports
export default createExpressApplication;
