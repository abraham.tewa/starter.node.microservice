/* eslint-env node, jest */
// ============================================================
// Import package
import request from 'supertest';

// ============================================================
// Import modules
import createExpressApplication from './createExpressApplication';

describe('createExpressApplication', () => {
    it('return a helmet secured application', async () => {
        const app = createExpressApplication();

        app.get('/status', (req, res) => res.send('ok'));

        const response = await request(app).get('/status');

        expect(response.headers).toHaveProperty('content-security-policy');
    });
});
