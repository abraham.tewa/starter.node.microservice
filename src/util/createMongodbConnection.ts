// ============================================================
// Import packages
import mongodb, { MongoClient } from 'mongodb';
import { ServiceConnection } from '../config/type';

// ============================================================
// Function
function createMongoDbConnection(config: ServiceConnection): MongoClient {
    const uri = config.connect;
    const options : mongodb.MongoClientOptions = {};

    if ('username' in config) {
        options.auth = {
            password: config.password,
            username: config.username,
        };
    }

    return new MongoClient(uri, options);
}

export default createMongoDbConnection;
