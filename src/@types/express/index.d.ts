import type { Logger } from 'winston';
import { Model } from '../../model';
import Service from '../../Service';
import { Process } from '../../types';

declare global {
    namespace Express {
        interface Request {
            model: Model,
            service: Service,
            logger: Logger
            process: Process,
            requestId: number,
        }
    }
}
