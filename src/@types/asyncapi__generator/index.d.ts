declare module '@asyncapi/generator' {
    import type {
        AsyncAPIDocument,
        ParserOptions,
    } from '@asyncapi/parser';

    class Generator {
        constructor(templateName: string, targetDir: string, options?: Partial<GeneratorOptions>);

        debug: boolean;

        disabledHooks: DisabledHooks;

        entrypoint: string;

        forceWrite: boolean;

        hooks: object;

        install: boolean;

        mapBaseUrlToFolder: boolean;

        noOverwriteGlobs: string[];

        originalAsyncAPI: string;

        output: string;

        targetDir: string;

        templateConfig: boolean;

        templateName: string;

        templateParams: object;

        generate(asyncapiDocument: AsyncAPIDocument) : Promise<this>;

        configureTemplate() : this;

        generateFromString(asyncapiString: string, options?: ParserOptions = {}): Promise<this>;

        generateFromURL(asyncapiURL: string): Promise<this>;

        generateFromFile(asyncapiFile: string): Promise<this>;

        installTemplate(force?: boolean = false): this;

        static getTemplateFile(templateName: string, filePath: string, templatesDir?: string): this;
    }

    type GeneratorOptions = {
        templateParams: string,
        entrypoint: string,
        noOverwriteGlobs: string[],
        disabledHooks: DisabledHooks,
        output: string,
        forceWrite: boolean,
        install: boolean,
        debug: boolean,
        mapBaseUrlToFolder: { [key: string]: string },
    };

    type DisabledHooks = { [key: string]: string | boolean | string[] };

    export default Generator;
}
