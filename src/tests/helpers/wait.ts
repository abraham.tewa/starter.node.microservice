// ============================================================
// Function
async function wait(duration: number) {
    return new Promise((fulfill) => {
        setTimeout(fulfill, duration);
    });
}

// ============================================================
// Exports
export default wait;
