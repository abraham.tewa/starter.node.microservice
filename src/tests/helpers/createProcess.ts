// ============================================================
// Import modules

import { InitializeProcessReturn, ProcessDeclaration } from '../../types';

// ============================================================
// Functions
function createProcess({
    spies,
} : CreateProcessOptions) : ProcessDeclaration<object> {
    return {
        validateConfig: spies.validateConfig || jest.fn((config) => ({ config })),
        initialize: (...args) => {
            spies.initialize?.(...args);
            return initialize(spies);
        },
    };
}

async function initialize(
    spies: CreateProcessOptionsSpies,
) : Promise<InitializeProcessReturn> {
    return {
        start: spies.start || jest.fn(),
        stop: spies.stop || jest.fn(),
        exposeManagementTools: spies.exposeManagementTools,
        exposeDocumentation: spies.exposeDocumentation,
    };
}

// ============================================================
// Types
type CreateProcessOptions = {
    spies: CreateProcessOptionsSpies
};

type CreateProcessOptionsSpies = {
    start?: jest.Mock,
    stop?: jest.Mock,
    initialize?: jest.Mock,
    exposeDocumentation?: jest.Mock,
    exposeManagementTools?: jest.Mock,
    validateConfig?: jest.Mock
};

// ============================================================
// Exports
export default createProcess;
export type {
    CreateProcessOptionsSpies as Spies,
};
