// ============================================================

import amqplib from 'amqplib';

let lastId = 0;

// Functions
function build() {
    const connectMock = jest.fn(buildConnection);

    lastId += 1;
    return {
        __esModule: true,
        default: {
            connect: connectMock,
            mock: {
                id: lastId,
            },
        },
    };
}

function buildConnection() {
    const methods = [
        'close',
    ];

    const createChannelMock = jest.fn(buildCreateChannel);
    lastId += 1;

    return {
        ...buildMockObject(methods),
        createChannel: createChannelMock,
        mock: {
            id: lastId,
        },
    };
}

function buildCreateChannel() {
    const methods = [
        'ack',
        'close',
        'consume',
        'nack',
        'prefetch',
    ];
    return buildMockObject(methods);
}

function buildMockObject(methods: string[]) {
    const mocks : jest.Mock[] = [];

    lastId += 1;

    return {
        ...Object.fromEntries(
            methods.map((name) => {
                const mock = jest.fn();
                mocks.push(mock);
                return [name, mock];
            }),
        ),
        mock: {
            id: lastId,
        },
    };
}

// ============================================================
// Types
type Mock = {
    connect: jest.Mock<Promise<Connection>>;
    clear: () => void,
    id: number,
};

interface Connection {
    close: jest.Mock<ReturnType<amqplib.Connection['close']>, Parameters<amqplib.Connection['close']>>,
    createChannel: jest.Mock<Promise<Channel>, Parameters<amqplib.Connection['createChannel']>>,
    mock: {
        clear: () => void,
        id: number,
    }
}

interface Channel {
    ack: jest.Mock<ReturnType<amqplib.Channel['ack']>, Parameters<amqplib.Channel['ack']>>,
    close: jest.Mock<ReturnType<amqplib.Channel['close']>, Parameters<amqplib.Channel['close']>>,
    consume: jest.Mock<ReturnType<amqplib.Channel['consume']>, Parameters<amqplib.Channel['consume']>>,
    nack: jest.Mock<ReturnType<amqplib.Channel['nack']>, Parameters<amqplib.Channel['nack']>>,
    prefetch: jest.Mock<ReturnType<amqplib.Channel['prefetch']>, Parameters<amqplib.Channel['prefetch']>>,
    mock: {
        clear: () => void,
        setNextValue: (name: string, value: unknown) => void,
        id: number,
    },
}

// ============================================================
// Exports
export {
    build,
};

export type {
    Mock,
};
