// ============================================================
// Import packages
import faker from '@faker-js/faker';
import merge from 'deepmerge';

// ============================================================
// Import modules
import { Type as ApplicationConfiguration } from '../../config';
import { LogFormat, LogLevel } from '../../enums';

// ============================================================
// Functions
function createConfig(
    mergeWith?: Partial<ApplicationConfiguration>,
) : ApplicationConfiguration {
    const config : ApplicationConfiguration = {
        port: faker.internet.port(),
        documentation: { expose: false },
        management: { expose: false },
        hostname: faker.internet.domainName(),
        logs: {
            format: faker.random.arrayElement(Object.values(LogFormat)),
            level: faker.random.arrayElement(Object.values(LogLevel)),
            transports: [],
        },
        services: {
            amqp: {
                connect: `amqp://${faker.internet.domainName}:${faker.internet.port()}`,
            },
            mongodb: {
                connect: `mongodb://${faker.internet.domainName}:${faker.internet.port()}`,
            },
        },
    };

    return mergeWith
        ? merge(config, mergeWith)
        : config;
}

// ============================================================
// Exports
export default createConfig;
