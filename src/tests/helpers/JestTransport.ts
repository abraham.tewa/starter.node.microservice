/* eslint-env node, jest */

// ============================================================
// Import packages
import winston from 'winston';
import WinstonTransport from 'winston-transport';

// ============================================================
// Functions
class JestTransport extends WinstonTransport {
    readonly #spy?: jest.Mock;

    constructor(spy?: jest.Mock) {
        super();

        this.#spy = spy;
    }

    override log(info: unknown, callback: () => void) {
        this.#spy?.(info);

        callback();
    }

    static createLogger(spy?: jest.Mock) {
        return winston.createLogger({
            transports: [
                new JestTransport(spy),
            ],
        });
    }
}

// ============================================================
// Exports
export default JestTransport;
