/* eslint-env node, jest */

// ============================================================
// Import packages
import { isPlainObject } from 'is-plain-object';
import faker from '@faker-js/faker';
import merge from 'deepmerge';

// ============================================================
// Import modules
import JestTransport from './JestTransport';
import ApplicationConfiguration from '../../config/type';
import { LogFormat, LogLevel } from '../../enums';
import Service from '../../Service';

// ============================================================
// Functions
function createService(
    loggerSpy?: jest.Mock,
    name?: string,
    config?: Partial<ApplicationConfiguration>,
) : Service {
    const transports = loggerSpy
        ? [new JestTransport(loggerSpy)]
        : [new JestTransport(jest.fn())];

    const defaultConfig : ApplicationConfiguration = {
        port: 80,
        hostname: 'localhost',
        logs: {
            level: LogLevel.info,
            format: LogFormat.json,
            transports,
        },
        management: {
            expose: false,
        },
        documentation: {
            expose: false,
        },
        services: {
            amqp: {
                connect: 'amqp://localhost:5672',
            },
            mongodb: {
                connect: 'mongodb://localhost:27017',
            },
        },
    };

    const finalConfig = config
        ? merge(
            defaultConfig,
            config,
            {
                isMergeableObject: isPlainObject,
            },
        )
        : defaultConfig;

    const service = new Service(
        finalConfig,
        name || faker.datatype.string(10),
    );

    return service;
}

// ============================================================
// Exports
export default createService;
