// ============================================================
// Import packages
import 'dotenv';
import path from 'path';
import nodeConfig from 'config';

// ============================================================
// Import modules
import * as config from '../../config';
import Service from '../../Service';
import serverProcess from '../../process/server';
import { OpenApiConfiguration } from '../../process/server/types';
import ApplicationConfiguration from '../../config/type';

// ============================================================
// Module's constants and variables
const command = 'start';

const desc = 'Start the server';

const builder = {
    config: {
        alias: 'c',
        type: 'string',
        normalize: true,
    },
    port: {
        alias: 'p',
        type: 'number',
    },
};

// ============================================================
// Functions

async function handler(argv: Argv) {
    const argvConfig : Partial<ApplicationConfiguration> = {};

    if (argv.port) {
        argvConfig.port = argv.port;
    }

    const loadedConfig = await config.load(argvConfig);

    const openapi : OpenApiConfiguration = {
        filePath: path.resolve('./openapi.yml'),
        validation: true,
        paths: loadedConfig.documentation.expose
            ? {
                json: '/openapi.json',
                ui: '/openapi',
                yaml: '/openapi.yml',
            } : undefined,
        rootPath: loadedConfig.documentation.expose
            ? loadedConfig.documentation.rootPath
            : undefined,
    };

    const service = new Service(
        loadedConfig,
        process.env.npm_package_name || 'app',
    );

    service.logger.info(
        'Configuration source',
        { configSource: nodeConfig.util.getConfigSources().map(({ name }) => name) },
    );
    service.logger.info('Configuration', config.stripUnsafe(loadedConfig));

    service.logger.info('Initialization...');

    await service.start(
        serverProcess,
        {
            dashboard: loadedConfig.management.expose
                ? {
                    path: '/dashboard',
                    rootPath: loadedConfig.management.rootPath,
                }
                : undefined,
            port: loadedConfig.port,
            hostname: loadedConfig.hostname,
            openapi,
        },
        'http',
    );

    service.logger.info('Service initialized');
}

type Argv = {
    config?: string,
    port?: number,
};

// ============================================================
// Exports
export {
    command,
    desc,
    builder,
    handler,
};
