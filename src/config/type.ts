// ============================================================
// Import packages
import * as WinstonTransport from 'winston/lib/winston/transports';
import Transport from 'winston-transport';

// ============================================================
// Import modules
import {
    LogFormat,
    LogLevel,
    LogType,
} from '../enums';

// ============================================================
// Types
type ApplicationConfiguration = {
    port?: number,
    hostname?: string,
    logs: {
        format: LogFormat,
        level: LogLevel,
        transports: Array<LogOptions | Transport>,
    },
    services: {
        amqp: ServiceConnection,
        mongodb: ServiceConnection,
    },
    management: ToolConfiguration,
    documentation: ToolConfiguration,
    processes?: {
        listener?: {
            concurrency: number,
            queue: string
        },
    },
};

interface ApplicationConfigurationSafe extends ApplicationConfiguration {
    services: {
        amqp: ServiceConnectionNoAuth,
        mongodb: ServiceConnectionNoAuth,
    }
}

type ServiceConnection = ServiceConnectionAuth | ServiceConnectionNoAuth;

type ServiceConnectionNoAuth = {
    connect: string,
};

interface ServiceConnectionAuth extends ServiceConnectionNoAuth {
    username: string,
    password: string,
}

type ToolConfiguration = ToolExposedConfiguration | ToolNOtExposedConfiguration;

type ToolNOtExposedConfiguration = {
    expose: false,
};

type ToolExposedConfiguration = {
    expose: true,
    rootPath: string,
};

interface FileTransportOptions extends Omit<WinstonTransport.FileTransportOptions, 'stream'> {
    type: LogType.file,
}

interface ConsoleTransportOptions extends WinstonTransport.ConsoleTransportOptions {
    type: LogType.console,
}

interface HttpTransportOptions extends Omit<WinstonTransport.HttpTransportOptions, 'agent'> {
    type: LogType.http,
}

type LogOptions = ConsoleTransportOptions | FileTransportOptions | HttpTransportOptions;

// ============================================================
// Exports
export default ApplicationConfiguration;

export {
    LogType,
};

export type {
    ApplicationConfiguration as Type,
    ApplicationConfigurationSafe as SafeType,
    FileTransportOptions,
    ConsoleTransportOptions,
    HttpTransportOptions,
    ToolConfiguration,
    ServiceConnection,
    ServiceConnectionAuth,
    ServiceConnectionNoAuth,
};
