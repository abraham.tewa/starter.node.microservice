/* eslint-env node, jest */
// ============================================================
// Import packages
import config from 'config';

// ============================================================
// Import modules
import load from './load';

declare module 'config' {
    interface IUtil {
        setNextValue(value: object): void;
    }
}

jest.mock('config', () => {
    const originalModule = jest.requireActual('config');
    let nextValue: object;

    return {
        __esModule: true,
        default: {
            util: {
                setNextValue(value: object) {
                    nextValue = value;
                },
                ...originalModule.util,
                toObject() {
                    return nextValue;
                },
            },
        },
    };
});

jest.mock('./schema', () => ({
    validate(obj: { isValid: boolean, next?: object }) {
        if (obj.isValid) {
            return {
                value: 'next' in obj ? obj.next : obj,
            };
        }
        return {
            error: new Error('Invalid object'),
        };
    },
}));

describe('load', () => {
    it('return loaded configuration', async () => {
        const initialConfig = { isValid: true };

        config.util.setNextValue(initialConfig);

        const loadedConfig = await load();

        expect(loadedConfig).toBe(initialConfig);
    });

    it('extend configuration', async () => {
        const initialConfig = { isValid: true };

        config.util.setNextValue(initialConfig);

        const loadedConfig = await load({ port: 80 });

        expect(loadedConfig).toStrictEqual({
            isValid: true,
            port: 80,
        });
    });

    it('throw an error if the object is not valid', async () => {
        config.util.setNextValue({ isValid: false });

        let error;

        try {
            await load();
        } catch (err) {
            error = err;
        }

        expect(error).toBeTruthy();
    });

    it('throw an error if no value', async () => {
        config.util.setNextValue({ isValid: true, next: undefined });

        let error;

        try {
            await load();
        } catch (err) {
            error = err;
        }

        expect(error).toBeTruthy();
    });
});
