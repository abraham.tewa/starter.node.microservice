// ============================================================
// Import packages
import config from 'config';

// ============================================================
// Import modules
import schema from './schema';
import { Type as Configuration } from './type';

// ============================================================
// Functions
async function load(extendWith?: Partial<Configuration>) : Promise<Configuration> {
    const obj = await config.util.toObject(config);

    if (extendWith) {
        config.util.extendDeep(obj, extendWith);
    }

    const {
        error,
        value,
    } = schema.validate(
        obj,
        {
            abortEarly: false,
            allowUnknown: false,
            convert: false,
        },
    );

    if (error) {
        throw error;
    }

    if (!value) {
        throw new Error('No configuration found');
    }

    return value;
}

// ============================================================
// Exports
export default load;
