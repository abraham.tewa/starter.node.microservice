// ============================================================
// Import packages
import Joi from 'joi';
import { LogFormat, LogLevel } from '../enums';

// ============================================================
// Import modules
import ApplicationConfiguration, {
    ConsoleTransportOptions, FileTransportOptions, HttpTransportOptions, LogType,
} from './type';

// ============================================================
// Schema

const RE_EXPRESS_PATH = /^\/.+[^/]$/;

const ServiceConnectionSafeSchema = Joi.object({
    connect: Joi.string(),
});

const ServiceConnectionSchema = Joi.alternatives(
    Joi.object({
        connect: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().required(),
    }),
    ServiceConnectionSafeSchema,
);

const schema = Joi.object<ApplicationConfiguration>({
    port: Joi.number().port(),

    hostname: Joi.string().hostname().when('port', {
        is: Joi.string().required(),
        then: Joi.string().hostname(),
        otherwise: Joi.optional(),
    }),

    services: Joi.object({
        amqp: ServiceConnectionSchema,
        mongodb: ServiceConnectionSchema,
    }).required(),

    logs: Joi.object({
        format: Joi.string().valid(...Object.values(LogFormat)),
        level: Joi.string().valid(...Object.values(LogLevel)),
        transports: Joi.array().items(Joi.alternatives(
            // Console
            Joi.object<ConsoleTransportOptions>({
                type: LogType.console,
                consoleWarnLevels: Joi.string(),
                stderrLevels: Joi.array().items(Joi.string()),
                debugStdout: Joi.boolean(),
                eol: Joi.string(),
            }),

            // File
            Joi.object<FileTransportOptions>({
                type: LogType.console,
                filename: Joi.string(),
                dirname: Joi.string(),
                options: Joi.object(),
                maxsize: Joi.number(),
                zippedArchive: Joi.boolean(),
                maxFiles: Joi.number(),
                eol: Joi.string(),
                tailable: Joi.boolean(),
            }),

            // Http
            Joi.object<HttpTransportOptions>({
                type: LogType.http,
                ssl: Joi.any(),
                host: Joi.string(),
                port: Joi.number().port(),
                auth: Joi.object({
                    username: Joi.string(),
                    password: Joi.string(),
                    bearer: Joi.string(),
                }),
                path: Joi.string(),
                headers: Joi.object(),
            }),
        )),
    }).required(),

    management: Joi.alternatives(
        Joi.object({
            expose: true,
            rootPath: Joi.string().default('/_').pattern(RE_EXPRESS_PATH),
        }),
        Joi.object({
            expose: false,
        }),
    ).required(),

    documentation: Joi.alternatives(
        Joi.object({
            expose: true,
            rootPath: Joi.string().default('/_/doc').pattern(RE_EXPRESS_PATH),
        }),
        Joi.object({
            expose: false,
        }),
    ),

    processes: Joi.object({
        listener: Joi.object({
            concurrency: Joi.number().min(0).integer().required(),
            queue: Joi.string().required(),
        }),
    }),
});

const safeSchema = schema.keys({
    services: {
        amqp: ServiceConnectionSafeSchema,
        mongodb: ServiceConnectionSafeSchema,
    },
});

// ============================================================
// Exports
export default schema;
export {
    safeSchema as safe,
};
