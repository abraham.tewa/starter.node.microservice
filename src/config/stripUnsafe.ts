// ============================================================
// Import modules
import { getUrlInfo } from '../util';
import { safe as safeSchema } from './schema';
import {
    Type as Configuration,
    SafeType as ConfigurationSafe,
} from './type';

function stripUnsafe(unsafe: Configuration) : ConfigurationSafe {
    const { value: safeConfig } = safeSchema.validate(
        unsafe,
        {
            abortEarly: false,
            stripUnknown: true,
        },
    );

    if (!safeConfig) {
        throw new Error('Unknown error');
    }

    safeConfig.services.amqp.connect = stripSensitiveFromConnectString(safeConfig.services.amqp.connect, 5672);
    safeConfig.services.mongodb.connect = stripSensitiveFromConnectString(safeConfig.services.mongodb.connect, 27017);

    return safeConfig;
}

function stripSensitiveFromConnectString(connectString: string, defaultPort?: number): string {
    const info = getUrlInfo(connectString, defaultPort);

    if (!info) {
        throw new Error('Invalid connect string');
    }

    const hostPart = info.hosts
        .map(({ host, port }) => `${host}:${port}`)
        .join(',');

    const path = info.path.length > 1
        ? info.path
        : '';

    const paramsStr = info.params.toString();

    const params = paramsStr.length
        ? `?${paramsStr}`
        : '';

    return `${info.protocol}://${hostPart}${path}${params}`;
}

// ============================================================
// Exports
export default stripUnsafe;
export {
    stripSensitiveFromConnectString,
};
