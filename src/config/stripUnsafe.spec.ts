/* eslint-env node, jest */
// ============================================================
// Import package
import faker from '@faker-js/faker';
import _ from 'lodash';

// ============================================================
// Import modules
import { createConfig } from '../tests/helpers';
import stripUnsafe from './stripUnsafe';
import ApplicationConfiguration from './type';

// ============================================================
// Tests
describe('stripUnsafe', () => {
    it('remove unsafe properties', () => {
        const amqpConnect = `amqp://${faker.internet.domainName()}:${faker.internet.port()}`;
        const mongodbConnect = `mongodb://${faker.internet.domainName()}:${faker.internet.port()}`;

        const unsafeConfig = createConfig({
            services: {
                amqp: {
                    connect: amqpConnect,
                    username: faker.internet.userName(),
                    password: faker.internet.password(),
                },
                mongodb: {
                    connect: mongodbConnect,
                    username: faker.internet.userName(),
                    password: faker.internet.password(),
                },
            },
        });

        const expectedConfig = _.omit(
            unsafeConfig,
            'services.amqp.username',
            'services.amqp.password',
            'services.mongodb.username',
            'services.mongodb.password',
        );

        const safeConfig = stripUnsafe(unsafeConfig);

        expect(safeConfig).not.toHaveProperty('services.amqp.username');
        expect(safeConfig).not.toHaveProperty('services.amqp.password');
        expect(safeConfig).not.toHaveProperty('services.mongodb.username');
        expect(safeConfig).not.toHaveProperty('services.mongodb.password');
        expect(safeConfig).toStrictEqual(expectedConfig);
    });

    it('remove unsafe auth', () => {
        const amqpAuth = `${faker.internet.userName()}:${faker.internet.password()}`;
        const amqpHostPort = `${faker.internet.domainName()}:${faker.internet.port()}`;

        const mongodbAuth = `${faker.internet.userName()}:${faker.internet.password()}`;
        const mongodbHostPort = `${faker.internet.domainName()}:${faker.internet.port()}`;

        const amqpConnect = `amqp://${amqpAuth}@${amqpHostPort}`;
        const mongodbConnect = `mongodb://${mongodbAuth}@${mongodbHostPort}`;

        const unsafeConfig = createConfig({
            services: {
                amqp: {
                    connect: amqpConnect,
                },
                mongodb: {
                    connect: mongodbConnect,
                },
            },
        });

        const expectedAmqpConnect = `amqp://${amqpHostPort}`;
        const expectedMongodbConnect = `mongodb://${mongodbHostPort}`;

        const safeConfig = stripUnsafe(unsafeConfig);

        expect(safeConfig.services.amqp.connect).toStrictEqual(expectedAmqpConnect);
        expect(safeConfig.services.mongodb.connect).toStrictEqual(expectedMongodbConnect);
    });

    it('include path if provided', () => {
        const amqpPath = `/${faker.lorem.word(5)}`;
        const mongodbPath = `/${faker.lorem.word(5)}`;

        const amqpAuth = `${faker.internet.userName()}:${faker.internet.password()}`;
        const amqpHostPort = `${faker.internet.domainName()}:${faker.internet.port()}`;

        const mongodbAuth = `${faker.internet.userName()}:${faker.internet.password()}`;
        const mongodbHostPort = `${faker.internet.domainName()}:${faker.internet.port()}`;

        const amqpConnect = `amqp://${amqpAuth}@${amqpHostPort}${amqpPath}`;
        const mongodbConnect = `mongodb://${mongodbAuth}@${mongodbHostPort}${mongodbPath}`;

        const unsafeConfig = createConfig({
            services: {
                amqp: {
                    connect: amqpConnect,
                },
                mongodb: {
                    connect: mongodbConnect,
                },
            },
        });

        const expectedAmqpConnect = `amqp://${amqpHostPort}${amqpPath}`;
        const expectedMongodbConnect = `mongodb://${mongodbHostPort}${mongodbPath}`;

        const safeConfig = stripUnsafe(unsafeConfig);

        expect(safeConfig.services.amqp.connect).toStrictEqual(expectedAmqpConnect);
        expect(safeConfig.services.mongodb.connect).toStrictEqual(expectedMongodbConnect);
    });

    it('include query string if provided', () => {
        const amqpPath = `/${faker.lorem.word(5)}`;
        const mongodbPath = `/${faker.lorem.word(5)}`;

        const amqpAuth = `${faker.internet.userName()}:${faker.internet.password()}`;
        const amqpHostPort = `${faker.internet.domainName()}:${faker.internet.port()}`;

        const mongodbAuth = `${faker.internet.userName()}:${faker.internet.password()}`;
        const mongodbHostPort = `${faker.internet.domainName()}:${faker.internet.port()}`;

        const amqpConnect = `amqp://${amqpAuth}@${amqpHostPort}${amqpPath}?param=value`;
        const mongodbConnect = `mongodb://${mongodbAuth}@${mongodbHostPort}${mongodbPath}?otherParam=otherValue`;

        const unsafeConfig = createConfig({
            services: {
                amqp: {
                    connect: amqpConnect,
                },
                mongodb: {
                    connect: mongodbConnect,
                },
            },
        });

        const expectedAmqpConnect = `amqp://${amqpHostPort}${amqpPath}?param=value`;
        const expectedMongodbConnect = `mongodb://${mongodbHostPort}${mongodbPath}?otherParam=otherValue`;

        const safeConfig = stripUnsafe(unsafeConfig);

        expect(safeConfig.services.amqp.connect).toStrictEqual(expectedAmqpConnect);
        expect(safeConfig.services.mongodb.connect).toStrictEqual(expectedMongodbConnect);
    });

    it('throw an error if safe configuration is not valid', () => {
        expect(() => {
            stripUnsafe('' as unknown as ApplicationConfiguration);
        }).toThrowError('Unknown error');
    });

    it('throw an error if connect string is not valid', () => {
        expect(() => {
            const configuration = createConfig({
                services: {
                    amqp: {
                        connect: 'fake',
                    },
                    mongodb: {
                        connect: 'fake',
                    },
                },
            });

            stripUnsafe(configuration);
        }).toThrowError('Invalid connect string');
    });
});
