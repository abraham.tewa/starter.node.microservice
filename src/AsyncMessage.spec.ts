/* eslint-env node, jest */
// ============================================================
// Import package
import faker from '@faker-js/faker';

// ============================================================
// Import modules
import { buildMessage, randomMessage } from './async/testHelper';
import { AsyncMessageEvent } from './AsyncMessage';
import { wait } from './tests/helpers';

// ============================================================
// Tests

describe('AsyncMessage', () => {
    it('create a promise resolved when message ack', async () => {
        const message = randomMessage();

        let resolved = false;

        message.promise.then(() => {
            resolved = true;
        });

        await wait(200);
        expect(resolved).toEqual(false);

        message.ack();

        await message.promise;

        expect(resolved).toEqual(true);
    });

    it('create a promise resolved when message nack', async () => {
        const message = randomMessage();

        let resolved = false;

        message.promise.then(() => {
            resolved = true;
        });

        await wait(200);
        expect(resolved).toEqual(false);

        message.nack();

        await message.promise;

        expect(resolved).toEqual(true);
    });

    it('throw an error if message already acked', () => {
        const message = randomMessage();
        message.ack();

        expect(() => message.ack()).toThrow('Message already acked/nacked');
        expect(() => message.nack()).toThrow('Message already acked/nacked');
    });

    it('throw an error if message already acked', () => {
        const message = randomMessage();
        message.ack();

        expect(() => message.ack()).toThrow('Message already acked/nacked');
        expect(() => message.nack()).toThrow('Message already acked/nacked');
    });

    it('return content', () => {
        const content = faker.datatype.string(10);

        const message = buildMessage(content);

        expect(message.content.toString()).toEqual(content);
    });

    it('return fields', () => {
        const message = randomMessage(false);

        expect(message.messageFields).toMatchSnapshot();
    });

    it('return properties', () => {
        const message = randomMessage();

        expect(message.messageProperties).toMatchSnapshot();
    });

    it('return a json clone', () => {
        const original = { test: 1 };
        const message = buildMessage(original);

        expect(message.json()).toStrictEqual(original);
        expect(message.json()).not.toBe(original);
        expect(message.json()).not.toBe(message.json());
    });

    describe('ack/nack', () => {
        const date = new Date(2022, 12, 2, 14, 40, 20);

        beforeEach(() => {
            jest.useFakeTimers('modern');
            jest.setSystemTime(date);
        });

        afterEach(() => {
            jest.useRealTimers();
        });

        it('set several properties on ack', () => {
            const message = randomMessage();

            const ackListener = jest.fn();
            const nackListener = jest.fn();

            message.addListener(AsyncMessageEvent.ack, ackListener);
            message.addListener(AsyncMessageEvent.nack, nackListener);

            message.ack();

            expect(message.ackedAt).toEqual(date);
            expect(message.isAcked()).toEqual(true);
            expect(message.isAckedOrNacked()).toEqual(true);

            // Listener call
            expect(ackListener).toBeCalledTimes(1);
            expect(ackListener.mock.calls[0][0]).toEqual({
                date: message.ackedAt,
                message,
            });

            // Is not nacked
            expect(message.nackedAt).toEqual(undefined);
            expect(message.isNacked()).toEqual(false);
            expect(nackListener).not.toBeCalled();
        });

        it('set several properties on nack', () => {
            const message = randomMessage();

            const ackListener = jest.fn();
            const nackListener = jest.fn();

            message.addListener(AsyncMessageEvent.ack, ackListener);
            message.addListener(AsyncMessageEvent.nack, nackListener);

            message.nack();

            expect(message.nackedAt).toEqual(date);
            expect(message.isNacked()).toEqual(true);
            expect(message.isAckedOrNacked()).toEqual(true);

            // Listener call
            expect(nackListener).toBeCalledTimes(1);
            expect(nackListener.mock.calls[0][0]).toEqual({
                date: message.nackedAt,
                message,
                requeue: false,
            });

            // Is not acked
            expect(message.ackedAt).toEqual(undefined);
            expect(message.isAcked()).toEqual(false);
            expect(ackListener).not.toBeCalled();
        });
    });
});
