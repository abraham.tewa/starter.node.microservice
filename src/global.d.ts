import { Document, ObjectId } from 'mongodb';

declare global {
    interface AppObject extends Document {
        id: string,
    }

    type DbInsert<T extends AppObject> = Omit<T, 'id'>;

    interface DbObject<T extends AppObject> extends Omit<T, 'id'> {
        _id: ObjectId,
    }
}
