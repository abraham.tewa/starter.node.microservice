// ============================================================
// Import packages
import { EventEmitter } from 'events';
import type {
    Message,
} from 'amqplib';
import { Logger } from 'winston';

// ============================================================
// Types
declare interface AsyncMessage<T> {
    on(event: AsyncMessageEvent.ack, listener: ((message: AckEventObject<T>) => void)): this,
    on(event: AsyncMessageEvent.nack, listener: ((message: NackEventObject<T>) => void)): this,
}

class AsyncMessage<T> extends EventEmitter {
    readonly logger: Logger;

    readonly message: Message;

    readonly promise: Promise<boolean>;

    readonly receivedAt: Date;

    #ackedAt?: Date;

    #nackedAt?: Date;

    #text?: string = undefined;

    #ackCb: () => void;

    #nackCb: (requeue: boolean) => void;

    #onfulfilled?: (acked: boolean) => void;

    constructor({
        ackCb,
        nackCb,
        message,
        receivedAt,
        logger,
    } : MessageConstructorParams) {
        super();

        this.message = message;
        this.logger = logger;
        this.receivedAt = receivedAt;
        this.#ackCb = ackCb;
        this.#nackCb = nackCb;

        this.promise = new Promise<boolean>(((onfulfilled) => {
            this.#onfulfilled = onfulfilled;
        }));
    }

    get ackedAt() {
        return this.#ackedAt;
    }

    get nackedAt() {
        return this.#nackedAt;
    }

    get content() {
        return this.message.content;
    }

    get messageFields() {
        return this.message.fields;
    }

    get messageProperties() {
        return this.message.properties;
    }

    ack() {
        if (this.isAckedOrNacked()) {
            throw new Error('Message already acked/nacked');
        }

        this.#ackedAt = new Date();
        this.#ackCb();
        this.#onfulfilled?.(true);
        this.emit(
            AsyncMessageEvent.ack,
            {
                date: new Date(this.#ackedAt),
                message: this,
            } as AckEventObject<T>,
        );
    }

    nack(requeue = false) {
        if (this.isAckedOrNacked()) {
            throw new Error('Message already acked/nacked');
        }

        this.#nackedAt = new Date();
        this.#nackCb(requeue);
        this.#onfulfilled?.(false);
        this.emit(
            AsyncMessageEvent.nack,
            {
                date: new Date(this.#nackedAt),
                message: this,
                requeue,
            } as NackEventObject<T>,
        );
    }

    isAckedOrNacked() {
        return Boolean(this.#ackedAt) || Boolean(this.#nackedAt);
    }

    isAcked() {
        return Boolean(this.#ackedAt);
    }

    isNacked() {
        return Boolean(this.#nackedAt);
    }

    json<U = T>(): U {
        return JSON.parse(this.text());
    }

    text(): string {
        if (this.#text === undefined) {
            this.#text = this.message.content.toString();
        }

        return this.#text;
    }
}

type MessageConstructorParams = {
    ackCb: () => void,
    nackCb: (requeue: boolean) => void,
    logger: Logger,
    message: Message
    receivedAt: Date,
};

enum AsyncMessageEvent {
    ack = 'ack',
    nack = 'nack',
}

type AckEventObject<T> = {
    date: Date,
    message: AsyncMessage<T>,
};

type NackEventObject<T> = {
    date: Date,
    message: AsyncMessage<T>,
    requeue: boolean,
};

// ============================================================
// Exports
export default AsyncMessage;
export {
    AsyncMessageEvent,
};
