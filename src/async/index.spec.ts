/* eslint-env node, jest */

// ============================================================
// Import modules
import { validateMessage } from '.';
import { buildMessage } from './testHelper';

// ============================================================
// Tests
describe('testHelper', () => {
    describe('buildMessage', () => {
        it('can build message from string', () => {
            const str = 'some string';
            const message = buildMessage(str);

            expect(message.text()).toBe(str);
        });

        it('can build message from buffer', () => {
            const str = 'some string';
            const message = buildMessage(Buffer.from(str));
            expect(message.text()).toBe(str);
        });

        it('can build message from object', () => {
            const object = { test: 1 };
            const message = buildMessage(object);
            expect(message.json()).toEqual(object);
        });

        it('build a AsyncMessage instance with jest mock functions', () => {
            const message = buildMessage('message');

            message.ack();
        });
    });
});

describe('index', () => {
    describe('validateMessage', () => {
        it('throw an error if no type is present', () => {
            let error;

            try {
                const message = buildMessage({});
                validateMessage(message);
            } catch (err) {
                error = err;
            }

            expect(error).toBeTruthy();

            expect((error as Error).message).toBe('Property "type" not defined');
        });
    });
});
