/* eslint-env node, jest */
// ============================================================
// Import package
import {
    ConsumeMessage,
    ConsumeMessageFields,
    MessageFields,
    MessageProperties,
} from 'amqplib';
import winston from 'winston';
import faker from '@faker-js/faker';

// ============================================================
// Import modules
import { validateMessage } from './index';
import AsyncMessage from '../AsyncMessage';

// ============================================================
// Module's constants and variables
const defaultFields: MessageFields = {
    deliveryTag: 100,
    exchange: 'some-exchange',
    redelivered: false,
    routingKey: 'some routing key',
};

const defaultProperties : MessageProperties = {
    appId: undefined,
    clusterId: undefined,
    contentEncoding: undefined,
    contentType: undefined,
    correlationId: undefined,
    deliveryMode: undefined,
    expiration: undefined,
    headers: {
        'x-death': undefined,
        'x-first-death-exchange': undefined,
        'x-first-death-queue': undefined,
        'x-first-death-reason': undefined,
    },
    messageId: undefined,
    priority: undefined,
    replyTo: undefined,
    timestamp: undefined,
    type: undefined,
    userId: undefined,
};

// ============================================================
// Functions
function buildAmqpMessage<T>(
    message: Buffer | string | object | T,
    fields?: Partial<ConsumeMessageFields>,
    properties?: Partial<MessageProperties>,
) : ConsumeMessage {
    let content: Buffer;

    if (message instanceof Buffer) {
        content = message;
    } else if (typeof message === 'string') {
        content = Buffer.from(message);
    } else {
        content = Buffer.from(JSON.stringify(message));
    }

    return {
        content,
        fields: {
            consumerTag: faker.datatype.string(10),
            ...defaultFields,
            ...(fields || {}),
        },
        properties: {
            ...(properties || {}),
            ...defaultProperties,
            headers: {
                ...(properties?.headers || {}),
                ...defaultProperties.headers,
            },
        },
    };
}

function buildMessage<T>(
    message: Buffer | string | object | T,
    fields?: Partial<ConsumeMessageFields>,
    properties?: Partial<MessageProperties>,
    ack = jest.fn,
    nack = jest.fn,
) : AsyncMessage<T> {
    const amqpMessage = buildAmqpMessage(message, fields, properties);

    const asyncMessage = new AsyncMessage({
        ackCb: ack,
        nackCb: nack,
        logger: winston.createLogger(),
        message: amqpMessage,
        receivedAt: new Date(),
    });

    return asyncMessage;
}

function isValidMessage(object: object) {
    const message = buildMessage(object);

    try {
        validateMessage(message);
        return true;
    } catch (err) {
        return false;
    }
}

function randomAmqpMessage() : ConsumeMessage {
    const content = faker.datatype.string(100);
    return buildAmqpMessage(content);
}

function randomMessage(randomConsumerTag = true) : AsyncMessage<string> {
    const content = faker.datatype.string(100);

    const messageFields : Partial<ConsumeMessageFields> = {};

    if (!randomConsumerTag) {
        messageFields.consumerTag = 'some consumer tag';
    }

    return buildMessage(content, messageFields);
}

function shouldBeValidObject(object: object) {
    expect(isValidMessage(object)).toBe(true);
}

function shouldBeInvalidObject(object: object) {
    expect(isValidMessage(object)).toBe(false);
}

// ============================================================
// Exports
export {
    buildAmqpMessage,
    buildMessage,
    randomAmqpMessage,
    randomMessage,
    shouldBeValidObject,
    shouldBeInvalidObject,
};
