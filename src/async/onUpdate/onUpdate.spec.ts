/* eslint-env node, jest */

// ============================================================
// Import packages
import faker from '@faker-js/faker';

// ============================================================
// Import modules
import onEvent from '..';
import {
    buildMessage,
    shouldBeInvalidObject,
    shouldBeValidObject,
} from '../testHelper';
import { AsyncMessageType } from '../types';

// ============================================================
// Tests
describe('event: onLog', () => {
    it('should log the information', async () => {
        const text = faker.datatype.string(10);
        const message = buildMessage({
            type: AsyncMessageType.update,
            id: text,
        });

        const log = jest.spyOn(console, 'log').mockImplementation(() => {});

        await onEvent(message);

        expect(log).toBeCalledTimes(1);
        expect(log).toBeCalledWith(text);
    });

    it('should throw an error if invalid schema', async () => {
        shouldBeValidObject({
            type: AsyncMessageType.update,
            id: 'some text',
        });

        shouldBeInvalidObject({
            type: AsyncMessageType.update,
            text: 'some text',
        });

        shouldBeInvalidObject({
            type: AsyncMessageType.update,
            log: 123,
        });
    });
});
