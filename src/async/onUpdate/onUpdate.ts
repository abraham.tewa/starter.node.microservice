// ============================================================
// Import package
import Joi from 'joi';

// ============================================================
// Import modules
import AmqpMessage from '../../AsyncMessage';
import type { IAsyncMessage } from '../types';
import {
    AsyncMessageType,
    IAsyncMessageSchema,
} from '../types';

// ============================================================
// Schemas
const Schema = IAsyncMessageSchema.append<MessageContent>({
    id: Joi.string().required(),
});

// ============================================================
// Function
async function listener(message: AmqpMessage<MessageContent>) {
    const text = message.json().id;
    // eslint-disable-next-line no-console
    console.log(text);
}

// ============================================================
// Types
interface MessageContent extends IAsyncMessage<AsyncMessageType.update> {
    id: string
}

// ============================================================
// Exports
export {
    Schema,
    listener,
};

export type {
    MessageContent,
};
