// ============================================================
// Import package
import Joi from 'joi';

// ============================================================
// Import modules
import AsyncMessage from '../AsyncMessage';
import { AsyncMessageType } from './types';
import type {
    IAsyncMessage,
} from './types';

import * as LogHandler from './onLog';
import * as UpdateHandler from './onUpdate';

import type { MessageContent as LogType } from './onLog';
import type { MessageContent as UpdateType } from './onUpdate';

// ============================================================
// Module constants and variables
type AsyncMessageUnion = LogType | UpdateType;

const ListenerMap : IListenerMap = {
    [AsyncMessageType.update]: UpdateHandler.listener,
    [AsyncMessageType.log]: LogHandler.listener,
};

const SchemaMap : ISchemaMap = {
    [AsyncMessageType.update]: UpdateHandler.Schema,
    [AsyncMessageType.log]: LogHandler.Schema,
};

// ============================================================
// Functions
async function onEvent(msg: AsyncMessage<unknown>) {
    const message = validateMessage(msg);
    const { type } = message.json();

    await ListenerMap[type](message);
}

function validateMessage(msg: AsyncMessage<unknown>): AsyncMessage<AsyncMessageUnion> {
    const content = msg.json<AsyncMessageUnion>();

    if (!content.type) {
        throw new Error('Property "type" not defined');
    }

    const schema = SchemaMap[content.type];

    const {
        error,
    } = schema.validate(
        msg.json(),
        {
            abortEarly: false,
            allowUnknown: false,
            convert: false,
        },
    );

    if (error) {
        throw error;
    }

    return msg;
}

type ISchemaMap = {
    [Property in AsyncMessageType]: Joi.ObjectSchema<IAsyncMessage<Property>>
};

type IListenerMap = {
    [Property in AsyncMessageType]: (
        (message: AsyncMessage<IAsyncMessage<Property>>) => (Promise<void> | void)
    )
};

// ============================================================
// Exports
export default onEvent;
export {
    validateMessage,
};
