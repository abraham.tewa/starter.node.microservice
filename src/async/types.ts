import Joi from 'joi';

enum AsyncMessageType {
    log = 'log',
    update = 'update',
}

interface IAsyncMessage<T extends AsyncMessageType> {
    type: T,
}

const IAsyncMessageSchema = Joi.object<IAsyncMessage<AsyncMessageType>>({
    type: Joi.string().valid(...Object.values(AsyncMessageType)).required(),
});

export {
    AsyncMessageType,
    IAsyncMessageSchema,
};

export type {
    IAsyncMessage,
};
