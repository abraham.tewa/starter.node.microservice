// ============================================================
// Import package
import Joi from 'joi';

// ============================================================
// Import modules
import AsyncMessage from '../../AsyncMessage';
import {
    AsyncMessageType,
    IAsyncMessageSchema,
} from '../types';

import type {
    IAsyncMessage,
} from '../types';

// ============================================================
// Schemas
const Schema = IAsyncMessageSchema.append<MessageContent>({
    log: Joi.string().required(),
});

// ============================================================
// Function
async function listener(message: AsyncMessage<MessageContent>) {
    const text = message.json().log;
    // eslint-disable-next-line no-console
    console.log(text);
}

// ============================================================
// Types
interface MessageContent extends IAsyncMessage<AsyncMessageType.log> {
    log: string
}

// ============================================================
// Exports
export {
    listener,
    Schema,
};

export type {
    MessageContent,
};
