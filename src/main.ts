// ============================================================
// Import modules
import ApplicationConfiguration from './config/type';
import Service from './Service';
import type {
    IProcessConfig,
    ProcessDeclaration,
} from './types';

// ============================================================
// Module's constants and variables
let service : Service;

// ============================================================
// Functions
async function main<T extends IProcessConfig>(
    info: ProcessInfo<T>,
    serviceInfo: ServiceInfo,
    config: ApplicationConfiguration,
) {
    if (!service) {
        service = new Service(
            config,
            serviceInfo.name,
        );
    }

    await service.start(
        info.declaration,
        info.config,
        info.name,
    );
}

// ============================================================
// Import modules
type ProcessInfo<T extends IProcessConfig> = {
    declaration: ProcessDeclaration<T>,
    name: string,
    config: T,
};

type ServiceInfo = {
    name: string,
};

// ============================================================
// Exports
export default main;
