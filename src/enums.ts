enum AmqpProtocol {
    amqp = 'amqp',
    amqps = 'amqps',
}

enum MongoDbProtocol {
    mongodb = 'mongodb',
    mongodbSrv = 'mongodb+srv',
}

enum LogFormat {
    pretty = 'pretty',
    text = 'text',
    json = 'json',
}

enum LogLevel {
    critical = 'critical',
    error = 'error',
    warn = 'warn',
    info = 'info',
    verbose = 'verbose',
    trace = 'trace',
    debug = 'debug',
}

enum LogType {
    console = 'console',
    file = 'file',
    http = 'http',
}

enum ProcessType {
    server = 'server',
    listener = 'listener',
}

export {
    AmqpProtocol,
    LogFormat,
    LogLevel,
    LogType,
    MongoDbProtocol,
    ProcessType,
};
