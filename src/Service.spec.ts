/* eslint-env node, jest */
// ============================================================
// Import packages
import faker from '@faker-js/faker';
import { Router } from 'express';
import request from 'supertest';

// ============================================================
// Import modules
import {
    createService,
    createProcess,
} from './tests/helpers';
import { Spies } from './tests/helpers/createProcess';

describe('Service', () => {
    describe('logger', () => {
        it('include uuid and service name', () => {
            const spy = jest.fn();
            const name = faker.datatype.string(10);

            const service = createService(spy, name);

            service.logger.info('test');

            const log = spy.mock.calls[0][0];

            expect(log.uuid).toEqual(service.uuid);
            expect(log.service).toEqual(name);
        });
    });

    describe('createRouter', () => {
        it('throw an exception if path do not start with "\\"', () => {
            const service = createService();

            expect(() => service.createRouter('api')).toThrowError();
        });

        it('create a router on correct path', async () => {
            const responseText = faker.datatype.string(10);
            const rootPath = '/root';
            const getPath = '/path';

            const service = createService();
            const router = service.createRouter(rootPath);

            router.get(getPath, (req, res) => {
                res.send(responseText);
            });

            const response = await request(service.app).get(`${rootPath}${getPath}`);

            expect(response.text).toEqual(responseText);
        });
    });

    describe('Start process', () => {
        it('throw an error if configuration is not valid', async () => {
            const message = 'Invalid configuration';

            const validateConfig = jest.fn(() => ({
                errors: [new Error(message)],
            }));
            const name = faker.datatype.string(10);
            const service = createService();
            const process = createProcess({
                spies: {
                    validateConfig,
                },
            });

            let error;

            try {
                await service.start(process, {}, name);
            } catch (err) {
                error = err;
            }

            expect(error).toBeTruthy();
            expect((error as Error).message).toBe(message);
        });

        it('Accept string or Error instance as error', async () => {
            const msg1 = 'Invalid configuration';
            const msg2 = new Error('Other invalid configuration error');

            const validateConfig = jest.fn(() => ({
                errors: [msg1, msg2],
            }));
            const name = faker.datatype.string(10);
            const service = createService();
            const process = createProcess({
                spies: {
                    validateConfig,
                },
            });

            let error;

            try {
                await service.start(process, {}, name);
            } catch (err) {
                error = err;
            }

            expect(error).toBeTruthy();
            expect((error as Error).message).toBe(`${msg1}\n${msg2.message}`);
        });

        it('Start the process', async () => {
            const validateConfig = jest.fn(() => ({
                config: {},
            }));
            const name = faker.datatype.string(10);
            const service = createService();
            const process = createProcess({
                spies: {
                    validateConfig,
                },
            });

            let error;

            try {
                await service.start(process, {}, name);
            } catch (err) {
                error = err;
            }

            expect(error).toBeUndefined();
        });

        it('Use final configuration to start the process', async () => {
            const initialConfig = { initial: true, final: false };
            const finalConfig = { initial: false, final: true };
            const loggerSpy = jest.fn();

            const spies : Spies = {
                initialize: jest.fn(),
                start: jest.fn(),
                validateConfig: jest.fn(() => ({ config: finalConfig })),
            };

            const service = createService(loggerSpy);
            const process = createProcess({
                spies,
            });

            await service.start(process, initialConfig, faker.datatype.string(10));

            expect(spies.validateConfig).toBeCalledTimes(1);
            expect(spies.validateConfig).toHaveBeenCalledWith(initialConfig);
            expect(spies.initialize).toHaveBeenCalledTimes(1);
            expect(spies.initialize).toHaveBeenLastCalledWith(finalConfig, service, expect.anything());

            expect(spies.start).toHaveBeenCalledTimes(1);
            expect(spies.start).toHaveBeenCalledWith();
        });

        it('Include process name in logs', async () => {
            const processName = faker.datatype.string(10);
            const loggerSpy = jest.fn();

            const spies : Spies = {
                initialize: jest.fn(),
            };

            const service = createService(loggerSpy);
            const process = createProcess({ spies });

            await service.start(process, {}, processName);

            const processLogger = spies.initialize?.mock.calls[0][2];

            loggerSpy.mockClear();
            processLogger.info('fake message');

            expect(loggerSpy.mock.calls[0][0].process).toEqual(processName);
        });

        describe('documentation', () => {
            it('Do not expose if not requested', async () => {
                const spies : Spies = {
                    exposeDocumentation: jest.fn(),
                };

                const service = createService(
                    undefined,
                    undefined,
                    {
                        documentation: {
                            expose: false,
                        },
                    },
                );
                const process = createProcess({ spies });

                await service.start(process, {}, faker.datatype.string(10));

                expect(spies.exposeDocumentation).not.toHaveBeenCalled();
            });

            it('Expose on correct path', async () => {
                const responseText = faker.datatype.string(100);
                const path = '/some-path';
                const rootPath = '/root';

                const spies : Spies = {
                    exposeDocumentation: jest.fn((router: Router) => {
                        router.get(path, (req, res) => {
                            res.send(responseText);
                        });
                    }),
                };

                const service = createService(
                    undefined,
                    undefined,
                    {
                        documentation: {
                            expose: true,
                            rootPath,
                        },
                    },
                );

                await service.start(
                    createProcess({ spies }),
                    {},
                    faker.datatype.string(10),
                );

                const response = await request(service.app).get(`${rootPath}${path}`);

                expect(response.text).toEqual(responseText);
            });
        });

        describe('Management tools', () => {
            it('Do not expose if not requested', async () => {
                const spies : Spies = {
                    exposeManagementTools: jest.fn(),
                };

                const service = createService(
                    undefined,
                    undefined,
                    {
                        management: {
                            expose: false,
                        },
                    },
                );
                const process = createProcess({ spies });

                await service.start(process, {}, faker.datatype.string(10));

                expect(spies.exposeManagementTools).not.toHaveBeenCalled();
            });

            it('Expose on correct path', async () => {
                const responseText = faker.datatype.string(100);
                const path = '/some-path';
                const rootPath = '/root';

                const spies : Spies = {
                    exposeManagementTools: jest.fn((router: Router) => {
                        router.get(path, (req, res) => {
                            res.send(responseText);
                        });
                    }),
                };

                const service = createService(
                    undefined,
                    undefined,
                    {
                        management: {
                            expose: true,
                            rootPath,
                        },
                    },
                );

                await service.start(
                    createProcess({ spies }),
                    {},
                    faker.datatype.string(10),
                );

                const response = await request(service.app).get(`${rootPath}${path}`);

                expect(response.text).toEqual(responseText);
            });
        });
    });
});
