// ============================================================
// Import packages
import { Logger } from 'winston';
import type { Router } from 'express';

// ============================================================
// Import modules
import { AsyncMessageType } from './async/types';
import Service from './Service';
import {
    ConsoleTransportOptions,
    FileTransportOptions,
    HttpTransportOptions,
} from './config/type';
import { LogFormat, LogLevel } from './enums';

// ============================================================
// Declarations

type UUID = string;

type ServiceConfiguration = {
    logs: ServiceLogConfiguration,

    management: Partial<ServiceManagementConfiguration>,

    documentation: Partial<ServiceDocumentationConfiguration>,
};

type ServiceLogConfiguration = {
    /**
     * Level to use for logging.
     * Default: info
     */
    level: LogLevel,

    /**
     * Format to use for logging.
     * Default: "json" on production, "pretty" otherwise
     */
    format: LogFormat,

    transports: Array<FileTransportOptions | ConsoleTransportOptions | HttpTransportOptions>,
};

type ServiceManagementConfiguration = {
    /**
     * If true, the documentation of the service will be exposed.
     * Default: process.env.NODE_ENV !== "production"
     */
    expose: boolean,

    /**
     * Port on which expose the management tools.
     * Default: http server port
     */
    port: number,

    /**
     * URL path on which expose the management tools.
     * Default: "_/"
     */
    rootPath: string,
};

type ServiceDocumentationConfiguration = {
    /**
     * If true, the documentation of the service will be exposed.
     * Default: "management.expose" property.
     */
    expose: boolean,

    /**
     * If true, only the documentation of the current process will be exposed.
     * Default: false
     */
    processOnly: boolean,

    /**
     * Port on which expose the documentation.
     * Default: "management.rootPath" property
     */
    port: number,

    /**
     * URL path on which expose the documentation.
     * Default: "_/doc/"
     */
    rootPath: string,
};

interface Process<T extends IProcessConfig> {
    service: Service,
    logger: Logger,
    config: T,
    stop: () => Promise<boolean>,
    context: object,
}

type IProcessConfig = object;

interface InitializeProcessReturn {
    start: () => Promise<void> | void,
    stop: (graceTime: number) => Promise<boolean>,
    exposeDocumentation?: (router: Router) => Promise<void>,
    exposeManagementTools?: (router: Router) => Promise<void>,
}

interface InitializeProcessCb<T extends IProcessConfig> {
    (processConfig: T, service: Service, logger: Logger): Promise<InitializeProcessReturn>
}

interface ProcessDeclaration<T extends IProcessConfig> {
    initialize: InitializeProcessCb<T>,
    validateConfig: (config: T) => ProcessConfigValidationResult<T>,
}

interface ProcessConfigValidationResult<T extends IProcessConfig> {
    errors?: Array<Error | string>,
    config?: T,
}

interface IAsyncMessage<T extends AsyncMessageType> {
    type : T,
}

declare global {

}

type DynamicMeta = (req: Express.Request, res: Express.Response, err: Error) => object;

export type {
    IAsyncMessage,
    InitializeProcessCb,
    InitializeProcessReturn,
    IProcessConfig,
    DynamicMeta,
    Process,
    ProcessConfigValidationResult,
    ProcessDeclaration,
    ServiceConfiguration,
    ServiceDocumentationConfiguration,
    ServiceLogConfiguration,
    ServiceManagementConfiguration,
    UUID,
};
