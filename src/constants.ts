const APPLICATION_NAME = 'node_ms';
const DEFAULT_CONCURRENCY_JOBS = 10;

const CONNECT_STRING = /^(?:([^/?:#\s]+):\/\/)+(?:([^@/?#\s]+)@)?([^/?#\s]+)?(?:\/([^?#\s]*))?(?:[?]([^#\s]+))?\S*$/;

export {
    APPLICATION_NAME,
    DEFAULT_CONCURRENCY_JOBS,

    // Regex
    CONNECT_STRING,
};
