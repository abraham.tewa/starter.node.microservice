import type { Request, Response } from 'express';

function declare(req: Request, res: Response): void {
    res
        .contentType('text/html')
        .send('OK');
}

export default declare;
