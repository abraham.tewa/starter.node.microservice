/* eslint-env node, jest */

// ============================================================
// Import packages
import express from 'express';
import request from 'supertest';

// ============================================================
// Import modules
import declare from '..';

// ============================================================
// Tests
describe('date', () => {
    let app: express.Express;

    beforeEach(() => {
        jest.useFakeTimers('modern');
        jest.setSystemTime(new Date('2022-01-01T00:00:00.000Z'));

        app = express();
        declare(app);
    });

    afterEach(() => {
        jest.useRealTimers();
    });

    it('Should response to the GET method', async () => {
        jest.setSystemTime(new Date('2022-01-01T00:00:00.000Z'));

        const response = await request(app).get('/status');

        expect(response.statusCode).toBe(200);
        expect(response.text).toBe('OK');
        expect(response.headers['content-type']).toMatch(/^text\/html/);

        const { headers } = response;

        delete headers.date;

        expect(headers).toMatchSnapshot();
    });
});
