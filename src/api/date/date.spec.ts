/* eslint-env node, jest */

// ============================================================
// Import packages
import express from 'express';
import request from 'supertest';

// ============================================================
// Import modules
import declare from '..';

beforeAll(() => {
    jest.useFakeTimers('modern');
    jest.setSystemTime(new Date(2022, 12, 2, 14, 40, 20));
});

afterAll(() => {
    jest.useRealTimers();
});

// ============================================================
// Tests
describe('date', () => {
    let app: express.Express;

    beforeEach(() => {
        app = express();
        declare(app);
    });

    it('Should response to the GET method', async () => {
        const response = await request(app).get('/date');
        expect(response.statusCode).toBe(200);
        expect(response.body).toMatchSnapshot();
    });
});
