import type { Request, Response } from 'express';

function declare(req: Request, res: Response): void {
    res.json(new Date().toISOString());
}

export default declare;
