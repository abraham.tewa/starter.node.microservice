import express from 'express';

import createUser from './createUser';
import getUser from './getUser';

function declare(): express.Router {
    const router = express.Router();

    router.get('/:userId', getUser);
    router.post('/', createUser);

    return router;
}

export {
    declare,
};
