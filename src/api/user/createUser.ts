// ============================================================
// Import packages
import { Request, Response } from 'express';
import type * as users from '../../model/users';

// ============================================================
// Handler
async function create(
    req: Request<null, null, Body>,
    res: Response<users.Type>,
) {
    const user = req.body;

    const exists = Boolean(await req.model.users.findByUsername(user.username));

    if (exists) {
        res.status(403);
        return;
    }

    const createdUser = await req.model.users.create(user);

    res.json(createdUser);
}

type Body = Omit<users.Type, 'id' | 'createdAt' | 'lastModifiedAt'>;

// ============================================================
// Exports
export default create;
