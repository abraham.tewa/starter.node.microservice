// ============================================================
// Import packages
import { Request, Response } from 'express';
import type * as users from '../../model/users';

// ============================================================
// Handler
async function getUser(req: Request<{ userId: string }, null, users.Type>, res: Response) {
    const { userId } = req.params;

    const user = await req.model.users.findById(userId);

    if (!user) {
        res.status(404);
        return;
    }

    res.json(user);
}

// ============================================================
// Exports
export default getUser;
