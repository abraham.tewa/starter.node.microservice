import type { Router } from 'express';

import * as user from './user';
import status from './status';
import date from './date';

function declare(router: Router) {
    router.get('/date', date);
    router.get('/status', status);

    router.use('/user', user.declare());
}

export default declare;
